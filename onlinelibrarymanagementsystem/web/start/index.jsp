<%-- 
    Document   : index
    Created on : Jun 12, 2017, 4:53:15 PM
    Author     : razu-razu
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Online Library Management System</title>
        <meta name="description" content="">
        <meta name="keywords" content="">
        <link href="<%= request.getContextPath()%>/img/favicon.ico" rel="shortcut icon" type="image/x-icon">

        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath()%>/css/fonts.googleapis.com.css">

        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath()%>/css/jquery.bxslider.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath()%>/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath()%>/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath()%>/css/animate.css">
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath()%>/css/style.css">

        <style type="text/css">
            .bottom-line {
                width: 50%;
            }
        </style>
    </head>
    <body>

        <!---->
        <div class="loader"></div>
        <!---->

        <!---->
        <div id="myDiv">
            <!--HEADER-->
            <div class="header">
                <div class="bg-color">
                    <header id="main-header">
                        <nav class="navbar navbar-default navbar-fixed-top">
                            <div class="container">
                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                    <a class="navbar-brand" href="#">Public<span class="logo-dec">&nbsp;Library</span></a>
                                </div>
                                <div class="collapse navbar-collapse" id="myNavbar">
                                    <ul class="nav navbar-nav navbar-right">
                                        <li class="active"><a href="#main-header">Home</a></li>
                                        <li class=""><a href="#popular">Popular</a></li>
                                        <li class=""><a href="#latest">Latest</a></li>
                                        <li class=""><a href="#contact">Contact Us</a></li>
                                    </ul>
                                </div>
                            </div>
                        </nav>
                    </header>
                    <div class="wrapper">
                        <div class="container">
                            <div class="row">
                                <div class="banner-info text-center wow fadeIn delay-05s">
                                    <h1 class="bnr-title">We are help<span>to yours for knowledge</span></h1>
                                    <h2 class="bnr-sub-title">Starting a new journey!!</h2>
                                    <p class="bnr-para">We care<br> yours <br> </p>
                                    <div class="brn-btn">
                                        <a href="LoginPage" class="btn btn-download">Login</a>
                                        <a href="#" class="btn btn-more">Learn More</a>
                                    </div>
                                    <div class="overlay-detail">
                                        <a href="#feature"><i class="fa fa-angle-down"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--/ HEADER-->

            <!---->
            <section id="popular" class="section-padding wow fadeInUp delay-05s">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <h2 class="service-title pad-bt15">Our Most Popular Book</h2>
                            <hr class="bottom-line">
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12 portfolio-item padding-right-zero mr-btn-15">
                            <figure>
                                <img src="<%= request.getContextPath()%>/img/port01.jpg" class="img-responsive">
                                <figcaption>
                                    <h2>Project For Everyone</h2>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nost.</p>
                                </figcaption>
                            </figure>
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12 portfolio-item padding-right-zero mr-btn-15">
                            <figure>
                                <img src="<%= request.getContextPath()%>/img/port02.jpg" class="img-responsive">
                                <figcaption>
                                    <h2>Project For Everyone</h2>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nost.</p>
                                </figcaption>
                            </figure>
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12 portfolio-item padding-right-zero mr-btn-15">
                            <figure>
                                <img src="<%= request.getContextPath()%>/img/port03.jpg" class="img-responsive">
                                <figcaption>
                                    <h2>Project For Everyone</h2>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nost.</p>
                                </figcaption>
                            </figure>
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12 portfolio-item padding-right-zero mr-btn-15">
                            <figure>
                                <img src="<%= request.getContextPath()%>/img/port04.jpg" class="img-responsive">
                                <figcaption>
                                    <h2>Project For Everyone</h2>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nost.</p>
                                </figcaption>
                            </figure>
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12 portfolio-item padding-right-zero mr-btn-15">
                            <figure>
                                <img src="<%= request.getContextPath()%>/img/port05.jpg" class="img-responsive">
                                <figcaption>
                                    <h2>Project For Everyone</h2>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nost.</p>
                                </figcaption>
                            </figure>
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12 portfolio-item padding-right-zero mr-btn-15">
                            <figure>
                                <img src="<%= request.getContextPath()%>/img/port06.jpg" class="img-responsive">
                                <figcaption>
                                    <h2>Project For Everyone</h2>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nost.</p>
                                </figcaption>
                            </figure>
                        </div>
                    </div>
                </div>
            </section>
            <!---->

            <!---->
            <section id="latest" class="section-padding wow fadeInUp delay-05s">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <h2 class="service-title pad-bt15">Our Latest Book</h2>
                            <hr class="bottom-line">
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="blog-sec">
                                <div class="blog-img">
                                    <a href="">
                                        <img src="<%= request.getContextPath()%>/img/blog01.jpg" class="img-responsive">
                                    </a>
                                </div>
                                <div class="blog-info">
                                    <h2>This is Lorem ipsum heading.</h2>
                                    <div class="blog-comment">
                                        <p>Posted In: <span>Legal Advice</span></p>
                                        <p>
                                            <span><a href="#"><i class="fa fa-comments"></i></a> 15</span>
                                            <span><a href="#"><i class="fa fa-eye"></i></a> 11</span></p>
                                    </div>
                                    <p>We cannot expect people to have respect for laws and orders until we teach respect to those we have entrusted to enforce those laws all the time. we always want to help people cordially.</p>
                                    <a href="" class="read-more">Read more →</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="blog-sec">
                                <div class="blog-img">
                                    <a href="">
                                        <img src="<%= request.getContextPath()%>/img/blog02.jpg" class="img-responsive">
                                    </a>
                                </div>
                                <div class="blog-info">
                                    <h2>This is Lorem ipsum heading.</h2>
                                    <div class="blog-comment">
                                        <p>Posted In: <span>Legal Advice</span></p>
                                        <p>
                                            <span><a href="#"><i class="fa fa-comments"></i></a> 15</span>
                                            <span><a href="#"><i class="fa fa-eye"></i></a> 11</span></p>
                                    </div>
                                    <p>We cannot expect people to have respect for laws and orders until we teach respect to those we have entrusted to enforce those laws all the time. we always want to help people cordially.</p>
                                    <a href="" class="read-more">Read more →</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="blog-sec">
                                <div class="blog-img">
                                    <a href="">
                                        <img src="<%= request.getContextPath()%>/img/blog03.jpg" class="img-responsive">
                                    </a>
                                </div>
                                <div class="blog-info">
                                    <h2>This is Lorem ipsum heading.</h2>
                                    <div class="blog-comment">
                                        <p>Posted In: <span>Legal Advice</span></p>
                                        <p>
                                            <span><a href="#"><i class="fa fa-comments"></i></a> 15</span>
                                            <span><a href="#"><i class="fa fa-eye"></i></a> 11</span></p>
                                    </div>
                                    <p>We cannot expect people to have respect for laws and orders until we teach respect to those we have entrusted to enforce those laws all the time. we always want to help people cordially.</p>
                                    <a href="" class="read-more">Read more →</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!---->

            <!---->
            <section id="contact" class="section-padding wow fadeInUp delay-05s">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 text-center white">
                            <h2 class="service-title pad-bt15">Keep in touch with us</h2>
                            <!--<p class="sub-title pad-bt15">Bangladesh public <br>Library</p>-->
                            <hr class="bottom-line white-bg">
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="loction-info white">
                                <p><i class="fa fa-map-marker fa-fw pull-left fa-2x"></i>Dhanmondi<br> 32, Dhaka</p>
                                <p><i class="fa fa-envelope-o fa-fw pull-left fa-2x"></i>pbl@info.com</p>
                                <p><i class="fa fa-phone fa-fw pull-left fa-2x"></i>+8801834623824</p>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="contact-form">
                                <div id="sendmessage">Your message has been sent. Thank you!</div>
                                <div id="errormessage"></div>
                                <form action="" method="post" role="form" class="contactForm">
                                    <div class="col-md-6 padding-right-zero">
                                        <div class="form-group">
                                            <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                                            <div class="validation"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" />
                                            <div class="validation"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
                                            <div class="validation"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message"></textarea>
                                            <div class="validation"></div>
                                        </div>
                                        <button type="submit" class="btn btn-primary btn-submit">SEND NOW</button>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!---->

            <!---->
            <footer id="footer">
                <div class="container">
                    <div class="row text-center">
                        <p>&copy; Public Library. All Rights Reserved.</p>
                    </div>
                </div>
            </footer>
            <!---->
        </div>
        <!---->

        <!--[if !IE]> -->
        <script src="<%= request.getContextPath()%>/js/jquery-2.1.4.min.js"></script>

        <script src="<%= request.getContextPath()%>/js/jquery.easing.min.js"></script>
        <script src="<%= request.getContextPath()%>/js/bootstrap.min.js"></script>
        <script src="<%= request.getContextPath()%>/js/wow.js"></script>
        <script src="<%= request.getContextPath()%>/js/custom.js"></script>

    </body>
</html>