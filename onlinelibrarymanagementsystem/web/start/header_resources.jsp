<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Online Library Management System</title>
<meta name="description" content="">
<meta name="keywords" content="">
<link href="<%= request.getContextPath()%>/img/favicon.ico" rel="shortcut icon" type="image/x-icon">
<!-- bootstrap & fontawesome -->
<link rel="stylesheet" type="text/css" href="<%= request.getContextPath()%>/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<%= request.getContextPath()%>/css/font-awesome.min.css">

<link rel="stylesheet" type="text/css" href="<%= request.getContextPath()%>/css/fonts.googleapis.com.css">
<link rel="stylesheet" type="text/css" href="<%= request.getContextPath()%>/css/ace.min.css">

<link rel="stylesheet" type="text/css" href="<%= request.getContextPath()%>/css/ace-skins.min.css">
<link rel="stylesheet" type="text/css" href="<%= request.getContextPath()%>/css/ace-rtl.min.css">

<script type="text/javascript" src="<%= request.getContextPath()%>/js/ace-extra.min.js"></script>