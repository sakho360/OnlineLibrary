package com.model;

import com.common.AllQueryStatement;
import com.persistance.UserGroup;
import com.persistance.UserInfo;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;

public class UserDao {

    public int newUser(
            Connection connection,
            String userid,
            String firstName,
            String lastName,
            String userName,
            String dob,
            Integer gender,
            String password,
            String mobile,
            String email,
            String occupation,
            String description,
            String userImg,
            String address,
            Integer groupid,
            String insertBy) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            try {

                UserInfo userInfo = new UserInfo();
                UserGroup group = new UserGroup();
                List<UserInfo> infos = new ArrayList<UserInfo>();
                userInfo.setUserid(userid);
                userInfo.setFirstName(firstName);
                userInfo.setLastName(lastName);
                userInfo.setUserName(userName);
                userInfo.setUserDob(dob);
                userInfo.setGender(gender);
                userInfo.setUserPassword(password);
                userInfo.setUserMobile(mobile);
                userInfo.setUserEmail(email);
                userInfo.setUserAddress(address);
                userInfo.setOccupation(occupation);
                userInfo.setDescription(description);
                userInfo.setUserImage(userImg);

                group.setUserGroupID(groupid);
                userInfo.setUserGroupInfo(group);

                userInfo.setInsertBy(insertBy);
                infos.add(userInfo);

                ps = connection.prepareStatement(AllQueryStatement.insertUser(infos));
                status = ps.executeUpdate();

            } catch (Exception e) {
                e.printStackTrace();
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (Exception ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }
}
