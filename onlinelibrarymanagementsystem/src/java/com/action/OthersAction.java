package com.action;

import com.common.AllList;
import com.common.DBConnection;
import com.common.EmailSender;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.FileUtils;
import org.apache.struts2.ServletActionContext;

public class OthersAction extends ActionSupport {

    private String menuParent;
    private String menuChlid;

    private String messageString;
    private String messageColor;

    private String recipient;
    private String subject;
    private String messageBody;

    private File attachment;
    private String attachmentFileName;

    private Connection connection = null;
    private AllList allList = null;

    public String sendMessage() {

        Map session = ActionContext.getContext().getSession();
        String userId = (String) session.get("USERID");

        if (userId == null || userId.length() == 0) {
            return LOGIN;
        }

        File fileToCreate = null;

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (allList == null) {
            allList = new AllList();
        }

        if (connection != null) {

            System.out.println("attachmentFileName " + attachmentFileName);

            String emailName = "Student Library";
            String fromEmail = "hasibalhasan1986@gmail.com";
            String upass = "bastopur.2013";

            try {
                // search for file     
                String destPath = ServletActionContext.getRequest().getSession().getServletContext().getRealPath("/EMAIL_FILE");
                fileToCreate = new File(destPath, attachmentFileName);
                FileUtils.copyFile(attachment, fileToCreate);
                String file = destPath + "\\" + attachmentFileName;

                if (EmailSender.sendAttachmentEmail(fromEmail, upass, recipient, subject, messageBody, file, emailName, attachmentFileName)) {
                    fileToCreate = new File(file);
                    if (fileToCreate.exists()) {
                        fileToCreate.delete();
                    }
                    setMessageColor("success");
                    setMessageString("Email sent successfully");
                } else {
                    fileToCreate = new File(file);
                    if (fileToCreate.exists()) {
                        fileToCreate.delete();
                    }
                    setMessageColor("danger");
                    setMessageString("Email sent failed");
                }
            } catch (IOException ex) {
                Logger.getLogger(OthersAction.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return SUCCESS;
    }

    public String goCalendarEventPage() {

        Map session = ActionContext.getContext().getSession();
        String userId = (String) session.get("USERID");

        if (userId == null || userId.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (allList == null) {
            allList = new AllList();
        }

        if (connection != null) {

        }

        setMenuParent("CalendarEv");

        return SUCCESS;
    }

    public String goGalleryEventPage() {

        Map session = ActionContext.getContext().getSession();
        String userId = (String) session.get("USERID");

        if (userId == null || userId.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (allList == null) {
            allList = new AllList();
        }

        if (connection != null) {

        }

        setMenuParent("Gallery");

        return SUCCESS;
    }

    public String goMessageEventPage() {

        Map session = ActionContext.getContext().getSession();
        String userId = (String) session.get("USERID");

        if (userId == null || userId.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (allList == null) {
            allList = new AllList();
        }

        if (connection != null) {

        }

        setMenuParent("Message");

        return SUCCESS;
    }

    /**
     * @return the menuParent
     */
    public String getMenuParent() {
        return menuParent;
    }

    /**
     * @param menuParent the menuParent to set
     */
    public void setMenuParent(String menuParent) {
        this.menuParent = menuParent;
    }

    /**
     * @return the recipient
     */
    public String getRecipient() {
        return recipient;
    }

    /**
     * @param recipient the recipient to set
     */
    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    /**
     * @return the subject
     */
    public String getSubject() {
        return subject;
    }

    /**
     * @param subject the subject to set
     */
    public void setSubject(String subject) {
        this.subject = subject;
    }

    /**
     * @return the messageBody
     */
    public String getMessageBody() {
        return messageBody;
    }

    /**
     * @param messageBody the messageBody to set
     */
    public void setMessageBody(String messageBody) {
        this.messageBody = messageBody;
    }

    /**
     * @return the messageString
     */
    public String getMessageString() {
        return messageString;
    }

    /**
     * @param messageString the messageString to set
     */
    public void setMessageString(String messageString) {
        this.messageString = messageString;
    }

    /**
     * @return the messageColor
     */
    public String getMessageColor() {
        return messageColor;
    }

    /**
     * @param messageColor the messageColor to set
     */
    public void setMessageColor(String messageColor) {
        this.messageColor = messageColor;
    }

    /**
     * @return the attachment
     */
    public File getAttachment() {
        return attachment;
    }

    /**
     * @param attachment the attachment to set
     */
    public void setAttachment(File attachment) {
        this.attachment = attachment;
    }

    /**
     * @return the attachmentFileName
     */
    public String getAttachmentFileName() {
        return attachmentFileName;
    }

    /**
     * @param attachmentFileName the attachmentFileName to set
     */
    public void setAttachmentFileName(String attachmentFileName) {
        this.attachmentFileName = attachmentFileName;
    }
}
