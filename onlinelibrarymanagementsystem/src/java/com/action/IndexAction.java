package com.action;

import com.common.AllList;
import com.common.DBConnection;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.persistance.BookInfo;
import com.persistance.UserInfo;
import com.persistance.UserLevelMenu;
import java.sql.Connection;
import java.util.List;
import java.util.Map;

public class IndexAction extends ActionSupport {

    private String userName;
    private String userPassword;

    private String menuParent;
    private String messageString;
    private String loginMessage;
    private String messageColor;

    private Connection connection = null;
    private AllList allList = null;

    public String login() {

        Map session = ActionContext.getContext().getSession();

        if (getUserName().isEmpty() || getUserPassword().isEmpty()) {
            setMessageColor("danger");
            setMessageString("Enter any User Name and password.");
            return INPUT;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (allList == null) {
            allList = new AllList();
        }

        if (connection != null) {

            List<UserInfo> userList = allList.loginInfo(connection, getUserName(), getUserPassword());

            if ((!userList.isEmpty()) && (userList.size() > 0)) {

                for (UserInfo user : userList) {

                    if ((user.getUserName().trim().equals(getUserName())) && (user.getUserPassword().trim().equals(getUserPassword()))) {

                        session.put("USERID", user.getUserid());
                        session.put("USERNAME", user.getUserName());
                        session.put("FULLNAME", user.getFullName());
                        session.put("GROUPID", user.getUserGroupInfo().getUserGroupID());
                        session.put("GROUPNAME", user.getUserGroupInfo().getGroupName());
                        session.put("USERIMG", user.getUserImage());

                        List<UserLevelMenu> menus = allList.getManus(connection, (String) session.get("USERID"), (Integer) session.get("GROUPID"));
                        for (UserLevelMenu menu : menus) {
                            session.put(menu.getMenuMaster().getMasterMenuID(), menu.getLevelMenuStatus());
                        }

                        if ((Integer) session.get("GROUPID") == 0) {
                            session.put("UBLIST", allList.getUserBookRentAlert(connection, "all", ""));
                        } else {
                            session.put("UBLIST", allList.getUserBookRentAlert(connection, "", (String) session.get("USERID")));
                        }

                        setLoginMessage("1");
                    } else {
                        setMessageColor("danger");
                        setMessageString("Invalid User Name or Password.");
                        return NONE;
                    }
                }
            } else {
                setMessageColor("danger");
                setMessageString("Invalid User Name or Password.");
                return ERROR;
            }
        } else {
            setMessageColor("danger");
            setMessageString("DB Connection not found !");
            return LOGIN;
        }

        return SUCCESS;
    }

    public String logout() {

        Map session = ActionContext.getContext().getSession();
        session.remove("USERID");
        session.remove("USERNAME");
        session.remove("FULLNAME");
        session.remove("GROUPID");
        session.remove("GROUPNAME");

        session.clear();

        return SUCCESS;
    }

    public String goLoginPage() {

        return SUCCESS;
    }

    public String goDashboardPage() {

        Map session = ActionContext.getContext().getSession();
        String userId = (String) session.get("USERID");

        if (userId == null || userId.length() == 0) {
            return LOGIN;
        }

        setMenuParent("Dashboard");

        return SUCCESS;
    }

    /**
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * @param userName the userName to set
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * @return the userPassword
     */
    public String getUserPassword() {
        return userPassword;
    }

    /**
     * @param userPassword the userPassword to set
     */
    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    /**
     * @return the messageString
     */
    public String getMessageString() {
        return messageString;
    }

    /**
     * @param messageString the messageString to set
     */
    public void setMessageString(String messageString) {
        this.messageString = messageString;
    }

    /**
     * @return the loginMessage
     */
    public String getLoginMessage() {
        return loginMessage;
    }

    /**
     * @param loginMessage the loginMessage to set
     */
    public void setLoginMessage(String loginMessage) {
        this.loginMessage = loginMessage;
    }

    /**
     * @return the messageColor
     */
    public String getMessageColor() {
        return messageColor;
    }

    /**
     * @param messageColor the messageColor to set
     */
    public void setMessageColor(String messageColor) {
        this.messageColor = messageColor;
    }

    /**
     * @return the menuParent
     */
    public String getMenuParent() {
        return menuParent;
    }

    /**
     * @param menuParent the menuParent to set
     */
    public void setMenuParent(String menuParent) {
        this.menuParent = menuParent;
    }
}
