package com.common;

import com.persistance.BookCategoryInfo;
import com.persistance.BookInfo;
import com.persistance.MenuChild;
import com.persistance.MenuMaster;
import com.persistance.UserGroup;
import com.persistance.UserInfo;
import com.persistance.UserLevelMenu;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class AllList {

    public List<UserInfo> loginInfo(Connection connection, String userName, String password) {

        UserInfo userInfo = null;
        UserGroup userGroupInfo = null;
        List<UserInfo> userInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(AllQueryStatement.selectLoginInfo(userName, password));
                rs = ps.executeQuery();

                userInfoList = new ArrayList<UserInfo>();

                while (rs.next()) {

                    userInfo = new UserInfo();
                    userGroupInfo = new UserGroup();

                    userInfo.setUserid(rs.getString("USER_ID"));
                    userInfo.setFullName(rs.getString("FIRST_NAME") + " " + rs.getString("LAST_NAME"));
                    userInfo.setUserName(rs.getString("USER_NAME"));
                    userInfo.setUserPassword(rs.getString("PASSWORD"));
                    userInfo.setUserMobile(rs.getString("MOBILE"));
                    userInfo.setUserImage(rs.getString("USER_IMG"));

                    userGroupInfo.setUserGroupID(rs.getInt("GROUP_ID"));
                    userGroupInfo.setGroupName(rs.getString("GROUP_NAME"));

                    userInfo.setUserGroupInfo(userGroupInfo);

                    userInfoList.add(userInfo);
                }
            } catch (Exception e) {
                e.printStackTrace();
                userInfoList = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return userInfoList;
    }

    public List<UserInfo> getUserInfo(Connection connection, String type, String gid, String id, String uid, String ubrent) {

        UserInfo userInfo = null;
        List<UserInfo> userInfoList = null;

        UserGroup userGroup = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(AllQueryStatement.selectUserInfo(type, gid, id, uid, ubrent));
                rs = ps.executeQuery();

                userInfoList = new ArrayList<UserInfo>();

                while (rs.next()) {

                    userInfo = new UserInfo();

                    userGroup = new UserGroup();

                    userInfo.setUserid(rs.getString("USER_ID"));
                    userInfo.setFullName(rs.getString("FIRST_NAME") + " " + rs.getString("LAST_NAME"));
                    userInfo.setFirstName(rs.getString("FIRST_NAME"));
                    userInfo.setLastName(rs.getString("LAST_NAME"));
                    userInfo.setUserName(rs.getString("USER_NAME"));
                    userInfo.setUserDob(rs.getString("DOB"));
                    userInfo.setUserPassword(rs.getString("PASSWORD"));
                    userInfo.setUserMobile(rs.getString("MOBILE"));
                    userInfo.setGender(rs.getInt("GENDER"));
                    userInfo.setUserEmail(rs.getString("EMAIL"));
                    userInfo.setUserAddress(rs.getString("ADDRESS"));
                    userInfo.setOccupation(rs.getString("OCCUPATION"));
                    userInfo.setDescription(rs.getString("DESCRIPTION"));
                    userInfo.setUserImage(rs.getString("USER_IMG"));
                    userInfo.setUserStatus(rs.getString("STATUS").charAt(0));

                    SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
                    Date date = rs.getDate("INSERT_DATE");
                    String sdate = formatter.format(date);
                    userInfo.setInsertDate(sdate);

                    userGroup.setUserGroupID(rs.getInt("GROUP_ID"));
                    userGroup.setGroupName(rs.getString("GROUP_NAME"));
                    userInfo.setUserGroupInfo(userGroup);

                    userInfoList.add(userInfo);
                }
            } catch (Exception e) {
                e.printStackTrace();
                userInfoList = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return userInfoList;
    }

    public List<UserGroup> getUserGroupInfo(Connection connection) {

        UserGroup userGroup = null;
        List<UserGroup> userGroups = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(AllQueryStatement.selectUserGoupInfo());
                rs = ps.executeQuery();

                userGroups = new ArrayList<UserGroup>();

                while (rs.next()) {

                    userGroup = new UserGroup();

                    userGroup.setUserGroupID(rs.getInt("GROUP_ID"));
                    userGroup.setGroupName(rs.getString("GROUP_NAME"));

                    userGroups.add(userGroup);
                }
            } catch (Exception e) {
                e.printStackTrace();
                userGroups = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return userGroups;
    }

    public List<UserInfo> searchUserInfo(Connection connection, String fName, String fValue) {

        UserInfo userInfo = null;
        List<UserInfo> userInfoList = null;

        UserGroup userGroup = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(AllQueryStatement.selectSearchUserInfo(fName, fValue));
                rs = ps.executeQuery();

                userInfoList = new ArrayList<UserInfo>();

                while (rs.next()) {

                    userInfo = new UserInfo();

                    userGroup = new UserGroup();

//                    SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
                    userInfo.setUserid(rs.getString("USER_ID"));
                    userInfo.setFullName(rs.getString("USER_FULL_NAME"));
                    userInfo.setUserName(rs.getString("USER_NAME"));
                    userInfo.setUserMobile(rs.getString("USER_PHONE"));
                    userInfo.setUserEmail(rs.getString("USER_EMAIL"));
                    userInfo.setUserStatus(rs.getString("USER_STATUS").charAt(0));
                    userInfo.setUserPassword(rs.getString("USER_PASSWORD"));
                    userInfo.setUserAddress(rs.getString("USER_ADDRESS"));

//                    Date dob = rs.getDate("USER_DOB");
//                    String dobDate = formatter.format(dob);
                    userInfo.setUserDob(rs.getString("USER_DOB"));

                    userGroup.setUserGroupID(rs.getInt("GROUP_ID"));
                    userGroup.setGroupName(rs.getString("GROUP_NAME"));
                    userInfo.setUserGroupInfo(userGroup);

                    userInfoList.add(userInfo);
                }
            } catch (Exception e) {
                e.printStackTrace();
                userInfoList = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return userInfoList;
    }

    public List<BookCategoryInfo> getBookCategoryInfo(Connection connection, String type, String uid, Integer bcatid) {

        BookCategoryInfo categoryInfo = null;
        List<BookCategoryInfo> categoryInfoList = null;

        UserInfo userInfo = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(AllQueryStatement.selectBookCategoryInfo(type, uid, bcatid));
                rs = ps.executeQuery();

                categoryInfoList = new ArrayList<BookCategoryInfo>();

                while (rs.next()) {

                    categoryInfo = new BookCategoryInfo();

                    userInfo = new UserInfo();

                    categoryInfo.setBookCategoryId(rs.getString("BOOK_CATEGORY_ID"));
                    categoryInfo.setBookCategoryName(rs.getString("BOOK_CATEGORY_NAME"));
                    categoryInfo.setCategoryDescription(rs.getString("DESCRIPTION"));
                    categoryInfo.setStatus(rs.getString("STATUS").charAt(0));
                    categoryInfo.setInsertBy(rs.getString("INSERT_BY"));

                    SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
                    Date date = rs.getDate("INSERT_DATE");
                    String sdate = formatter.format(date);
                    categoryInfo.setInsertDate(sdate);

                    userInfo.setFullName(rs.getString("FIRST_NAME") + " " + rs.getString("LAST_NAME"));
                    categoryInfo.setUserInfo(userInfo);

                    categoryInfoList.add(categoryInfo);
                }
            } catch (Exception e) {
                e.printStackTrace();
                categoryInfoList = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return categoryInfoList;
    }

    public List<BookInfo> getBookInfo(Connection connection, String type, String uid, String bid) {

        BookInfo bookInfo = null;
        List<BookInfo> bookInfos = null;

        UserInfo userInfo = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(AllQueryStatement.selectBookInfo(type, uid, bid));
                rs = ps.executeQuery();

                bookInfos = new ArrayList<BookInfo>();

                while (rs.next()) {

                    bookInfo = new BookInfo();

                    userInfo = new UserInfo();

                    SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");

                    bookInfo.setBookId(rs.getString("BOOK_ID"));
                    bookInfo.setBookRef(rs.getString("BOOK_REF"));
                    bookInfo.setBookName(rs.getString("BOOK_NAME"));
                    bookInfo.setBookTitle(rs.getString("BOOK_TITLE"));
                    bookInfo.setBookQuantity(rs.getInt("BOOK_QUANTITY"));
                    bookInfo.setBookLocation(rs.getString("BOOK_LOCATION"));
                    bookInfo.setDescription(rs.getString("BOOK_DESCRIPTION"));
                    bookInfo.setCategory(rs.getString("BOOK_CATEGORY"));
                    bookInfo.setImgUrl(rs.getString("IMG_URL"));
                    bookInfo.setStatus(rs.getString("BOOK_STATUS").charAt(0));
                    Date date = rs.getDate("INSERT_DATE");
                    String upDate = formatter.format(date);
                    bookInfo.setInsertDate(upDate);

                    bookInfo.setBookRent(rs.getInt("RENT"));
                    bookInfo.setBookCurStock(rs.getInt("BOOK_QUANTITY") - rs.getInt("RENT"));

                    bookInfos.add(bookInfo);
                }
            } catch (Exception e) {
                e.printStackTrace();
                bookInfos = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return bookInfos;
    }

    /**
     * ******************************** Menu List Start
     */
    public List<UserLevelMenu> getManus(Connection connection, String userId, Integer groupId) {

        UserLevelMenu userLevelMenuInfo = null;
        List<UserLevelMenu> userLevelMenusList = null;

        MenuMaster menuMaster = null;
        UserInfo userInfo = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                String selectMenu = " SELECT "
                        + " ULM.USER_LEVEL_MENU_ID, "
                        + " MN.MENU_ID, "
                        + " MN.MENU_NAME, "
                        + " UI.USER_ID, "
                        + " UI.USER_NAME, "
                        + " CASE "
                        + " WHEN "
                        + " ULM.USER_LEVEL_MENU_STATUS = 'Y' "
                        + " THEN 'T' ELSE 'F' "
                        + " END "
                        + " USER_LEVEL_MENU_STATUS "
                        + " FROM "
                        + " user_level_menu ULM, "
                        + " menu MN, "
                        + " user_info UI "
                        + " WHERE "
                        + " ULM.MENU_ID = MN.MENU_ID "
                        + " AND "
                        + " ULM.GROUP_ID = UI.GROUP_ID "
                        + " AND "
                        + " ULM.GROUP_ID = '" + groupId + "' "
                        + " AND "
                        + " UI.USER_ID = '" + userId + "' ";

                ps = connection.prepareStatement(selectMenu);
                rs = ps.executeQuery();

                userLevelMenusList = new ArrayList<UserLevelMenu>();

                while (rs.next()) {

                    userLevelMenuInfo = new UserLevelMenu();

                    menuMaster = new MenuMaster();
                    userInfo = new UserInfo();

                    userLevelMenuInfo.setLevelMenuStatus(rs.getString("USER_LEVEL_MENU_STATUS").charAt(0));

                    menuMaster.setMasterMenuID(rs.getInt("MENU_ID"));
                    userLevelMenuInfo.setMenuMaster(menuMaster);

                    userLevelMenusList.add(userLevelMenuInfo);
                }
            } catch (Exception e) {
                userLevelMenusList = null;
                System.out.println(e);
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return userLevelMenusList;
    }

    public List<UserLevelMenu> getMenuByGroup(Connection connection, String groupID) {

        UserLevelMenu ulMenuInfo = null;
        List<UserLevelMenu> userLevelMenuInfoList = null;

        MenuMaster menuMaster = null;
        UserGroup userGroup = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                String userLevelMenuSelectStatement = " SELECT "
                        + " ULM.USER_LEVEL_MENU_ID, "
                        + " ULM.USER_LEVEL_MENU_STATUS, "
                        + " MN.MENU_ID, "
                        + " MN.MENU_NAME, "
                        + " MN.PARENT_MENU_ID, "
                        + " UG.GROUP_ID, "
                        + " UG.GROUP_NAME "
                        + " FROM "
                        + " user_level_menu ULM, "
                        + " menu MN, "
                        + " user_group UG "
                        + " WHERE "
                        + " ULM.MENU_ID = MN.MENU_ID "
                        + " AND "
                        + " ULM.GROUP_ID = UG.GROUP_ID "
                        + " AND "
                        + " UG.GROUP_ID = '" + groupID + "' "
                        + " AND "
                        + " MN.PARENT_MENU_ID = 0 ";

                ps = connection.prepareStatement(userLevelMenuSelectStatement);
                rs = ps.executeQuery();

                userLevelMenuInfoList = new ArrayList<UserLevelMenu>();

                while (rs.next()) {

                    ulMenuInfo = new UserLevelMenu();

                    menuMaster = new MenuMaster();
                    userGroup = new UserGroup();

                    ulMenuInfo.setUserLevelMenuID(rs.getInt("USER_LEVEL_MENU_ID"));
                    ulMenuInfo.setLevelMenuStatus(rs.getString("USER_LEVEL_MENU_STATUS").charAt(0));

                    menuMaster.setMasterMenuID(rs.getInt("MENU_ID"));
                    menuMaster.setMasterMenuName(rs.getString("MENU_NAME"));
                    menuMaster.setMasterParentMenuID(rs.getInt("PARENT_MENU_ID"));
                    ulMenuInfo.setMenuMaster(menuMaster);

                    userGroup.setUserGroupID(rs.getInt("GROUP_ID"));
                    userGroup.setGroupName(rs.getString("GROUP_NAME"));
                    ulMenuInfo.setUserGroupInfo(userGroup);

                    ulMenuInfo.setUserLevelMenuChildList(getMenuChildByGroup(connection, rs.getInt("GROUP_ID"), rs.getInt("MENU_ID")));

                    userLevelMenuInfoList.add(ulMenuInfo);
                }
            } catch (Exception e) {
                e.printStackTrace();
                userLevelMenuInfoList = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return userLevelMenuInfoList;
    }

    public List<MenuChild> getMenuChildByGroup(Connection connection, Integer groupID, Integer pmID) {

        MenuChild menuChild = null;
        List<MenuChild> menuChildInfoList = null;

        UserGroup userGroup = null;
        UserLevelMenu userLevelMenuInfo = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                String userLevelChildMenuSelectStatement = " SELECT "
                        + " ULM.USER_LEVEL_MENU_ID, "
                        + " ULM.USER_LEVEL_MENU_STATUS, "
                        + " MN.MENU_ID, "
                        + " MN.MENU_NAME, "
                        + " MN.PARENT_MENU_ID, "
                        + " UG.GROUP_ID, "
                        + " UG.GROUP_NAME "
                        + " FROM "
                        + " user_level_menu ULM, "
                        + " menu MN, "
                        + " user_group UG "
                        + " WHERE "
                        + " ULM.MENU_ID = MN.MENU_ID "
                        + " AND "
                        + " ULM.GROUP_ID = UG.GROUP_ID "
                        + " AND "
                        + " UG.GROUP_ID = '" + groupID + "' "
                        + " AND "
                        + " MN.PARENT_MENU_ID = '" + pmID + "' ";

                ps = connection.prepareStatement(userLevelChildMenuSelectStatement);
                rs = ps.executeQuery();

                menuChildInfoList = new ArrayList<MenuChild>();

                while (rs.next()) {

                    menuChild = new MenuChild();

                    userGroup = new UserGroup();
                    userLevelMenuInfo = new UserLevelMenu();

                    menuChild.setChildMenuID(rs.getInt("MENU_ID"));
                    menuChild.setChildMenuName(rs.getString("MENU_NAME"));
                    menuChild.setChildParentMenuID(rs.getInt("PARENT_MENU_ID"));

                    userGroup.setUserGroupID(rs.getInt("GROUP_ID"));
                    userGroup.setGroupName(rs.getString("GROUP_NAME"));
                    userLevelMenuInfo.setUserGroupInfo(userGroup);

                    userLevelMenuInfo.setUserLevelMenuID(rs.getInt("USER_LEVEL_MENU_ID"));
                    userLevelMenuInfo.setLevelMenuStatus(rs.getString("USER_LEVEL_MENU_STATUS").charAt(0));
                    menuChild.setUserLevelMenu(userLevelMenuInfo);

                    menuChildInfoList.add(menuChild);
                }
            } catch (Exception e) {
                System.out.println(e);
                menuChildInfoList = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return menuChildInfoList;
    }

    public List<MenuMaster> getMenuParentInfo(Connection connection) {

        MenuMaster menuMaster = null;
        List<MenuMaster> menuMasterInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                String menuMasterSelectStatement = " SELECT "
                        + " MENU_ID, "
                        + " MENU_NAME, "
                        + " MENU_TITLE, "
                        + " PARENT_MENU_ID, "
                        + " MENU_STATUS "
                        + " FROM "
                        + " menu "
                        + " WHERE "
                        + " PARENT_MENU_ID = 0 ";

                ps = connection.prepareStatement(menuMasterSelectStatement);
                rs = ps.executeQuery();

                menuMasterInfoList = new ArrayList<MenuMaster>();

                while (rs.next()) {

                    menuMaster = new MenuMaster();

                    menuMaster.setMasterMenuID(rs.getInt("MENU_ID"));
                    menuMaster.setMasterMenuName(rs.getString("MENU_NAME"));
                    menuMaster.setMasterMenuTitle(rs.getString("MENU_TITLE"));
                    menuMaster.setMasterParentMenuID(rs.getInt("PARENT_MENU_ID"));
                    menuMaster.setMasterMenuStatus(rs.getString("MENU_STATUS").charAt(0));

                    menuMaster.setMenuChildList(getMenuChildInfo(connection, rs.getInt("MENU_ID")));

                    menuMasterInfoList.add(menuMaster);
                }
            } catch (Exception e) {
                System.out.println(e);
                menuMasterInfoList = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return menuMasterInfoList;
    }

    public List<MenuChild> getMenuChildInfo(Connection connection, Integer masterMenuID) {

        MenuChild menuChild = null;
        List<MenuChild> menuChildInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                String menuChildSelectStatement = " SELECT "
                        + " MENU_ID, "
                        + " MENU_NAME, "
                        + " MENU_TITLE, "
                        + " PARENT_MENU_ID, "
                        + " MENU_STATUS "
                        + " FROM "
                        + " menu "
                        + " WHERE "
                        + " PARENT_MENU_ID = '" + masterMenuID + "' ";

                ps = connection.prepareStatement(menuChildSelectStatement);
                rs = ps.executeQuery();

                menuChildInfoList = new ArrayList<MenuChild>();

                while (rs.next()) {

                    menuChild = new MenuChild();

                    menuChild.setChildMenuID(rs.getInt("MENU_ID"));
                    menuChild.setChildMenuName(rs.getString("MENU_NAME"));
                    menuChild.setChildMenuTitle(rs.getString("MENU_TITLE"));
                    menuChild.setChildParentMenuID(rs.getInt("PARENT_MENU_ID"));
                    menuChild.setChildMenuStatus(rs.getString("MENU_STATUS").charAt(0));

                    menuChildInfoList.add(menuChild);
                }
            } catch (Exception e) {
                System.out.println(e);
                menuChildInfoList = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return menuChildInfoList;
    }

    /**
     * ******************************** Menu List End
     */
    public List<UserInfo> getBookRentByUser(Connection connection, String type, String id, String uid) {

        UserInfo userInfo = null;
        List<UserInfo> userInfoList = null;

        UserGroup userGroup = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(AllQueryStatement.selectBookRentByUser(type, id, uid));
                rs = ps.executeQuery();

                userInfoList = new ArrayList<UserInfo>();

                while (rs.next()) {

                    userInfo = new UserInfo();

                    userGroup = new UserGroup();

                    userInfo.setUserid(rs.getString("USER_ID"));
                    userInfo.setFullName(rs.getString("FIRST_NAME") + " " + rs.getString("LAST_NAME"));
                    userInfo.setFirstName(rs.getString("FIRST_NAME"));
                    userInfo.setLastName(rs.getString("LAST_NAME"));
                    userInfo.setUserName(rs.getString("USER_NAME"));
                    userInfo.setUserDob(rs.getString("DOB"));
                    userInfo.setUserPassword(rs.getString("PASSWORD"));
                    userInfo.setUserMobile(rs.getString("MOBILE"));
                    userInfo.setGender(rs.getInt("GENDER"));
                    userInfo.setUserEmail(rs.getString("EMAIL"));
                    userInfo.setUserAddress(rs.getString("ADDRESS"));
                    userInfo.setOccupation(rs.getString("OCCUPATION"));
                    userInfo.setDescription(rs.getString("DESCRIPTION"));
                    userInfo.setUserImage(rs.getString("USER_IMG"));
                    userInfo.setUserStatus(rs.getString("STATUS") == null ? "N".charAt(0) : rs.getString("STATUS").charAt(0));

                    SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
                    Date date = rs.getDate("INSERT_DATE");
                    if (date != null) {
                        String sdate = formatter.format(date);
                        userInfo.setInsertDate(sdate);
                    } else {
                        userInfo.setInsertDate("");
                    }

//                    userGroup.setUserGroupID(rs.getInt("GROUP_ID"));
//                    userGroup.setGroupName(rs.getString("GROUP_NAME"));
//                    userInfo.setUserGroupInfo(userGroup);
                    userInfo.setBookInfos(getRentBookInfo(connection, rs.getString("USER_ID")));

                    userInfoList.add(userInfo);
                }
            } catch (Exception e) {
                e.printStackTrace();
                userInfoList = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return userInfoList;
    }

    public List<BookInfo> getRentBookInfo(Connection connection, String uid) {

        BookInfo bookInfo = null;
        List<BookInfo> bookInfos = null;

        UserInfo userInfo = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(AllQueryStatement.selectRentBookInfo(uid));
                rs = ps.executeQuery();

                bookInfos = new ArrayList<BookInfo>();

                while (rs.next()) {

                    bookInfo = new BookInfo();

                    userInfo = new UserInfo();

                    SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");

                    bookInfo.setBookId(rs.getString("BOOK_ID"));
                    bookInfo.setBookRef(rs.getString("BOOK_REF"));
                    bookInfo.setBookName(rs.getString("BOOK_NAME"));
                    bookInfo.setBookTitle(rs.getString("BOOK_TITLE"));
                    bookInfo.setBookQuantity(rs.getInt("BOOK_QUANTITY"));
                    bookInfo.setBookRent(rs.getInt("BOOK_RENT_QUANTITY"));
                    bookInfo.setBookCurStock(rs.getInt("BOOK_QUANTITY") - rs.getInt("BOOK_RENT_QUANTITY"));
                    bookInfo.setCategory(rs.getString("BOOK_CATEGORY"));
                    bookInfo.setBookLocation(rs.getString("BOOK_LOCATION"));
                    bookInfo.setDescription(rs.getString("BOOK_DESCRIPTION"));
                    bookInfo.setImgUrl(rs.getString("IMG_URL"));
                    bookInfo.setStatus(rs.getString("BOOK_STATUS").charAt(0));

                    bookInfo.setRentDate(rs.getString("BOOK_RENT_DATE"));
                    bookInfo.setReturnDate(rs.getString("BOOK_RERUTN_DATE"));

                    Calendar returnCal = Calendar.getInstance();
                    returnCal.setTime(rs.getTimestamp("BOOK_RERUTN_DATE"));

                    Calendar rentCal = Calendar.getInstance();
                    rentCal.setTime(rs.getTimestamp("BOOK_RENT_DATE"));

                    Calendar cal = Calendar.getInstance();
                    Date today = cal.getTime();
                    System.out.println("today " + today);

                    long rentDays = (returnCal.getTimeInMillis() - rentCal.getTimeInMillis()) / (1000 * 60 * 60 * 24);
                    bookInfo.setBookRentDays(rentDays);

                    bookInfos.add(bookInfo);
                }
            } catch (Exception e) {
                e.printStackTrace();
                bookInfos = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return bookInfos;
    }

    /**
     * ********************* for alert
     */
    public List<UserInfo> getUserBookRentAlert(Connection connection, String type, String pid) {

        UserInfo userInfo = null;
        List<UserInfo> userInfoList = null;

        UserGroup userGroup = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(AllQueryStatement.selectUserBookRentAlert(type, pid));
                rs = ps.executeQuery();

                userInfoList = new ArrayList<UserInfo>();

                while (rs.next()) {

                    userInfo = new UserInfo();

                    userGroup = new UserGroup();

                    userInfo.setUserid(rs.getString("USER_ID"));
                    userInfo.setFullName(rs.getString("FIRST_NAME") + " " + rs.getString("LAST_NAME"));
                    userInfo.setFirstName(rs.getString("FIRST_NAME"));
                    userInfo.setLastName(rs.getString("LAST_NAME"));
                    userInfo.setUserName(rs.getString("USER_NAME"));

//                    userGroup.setUserGroupID(rs.getInt("GROUP_ID"));
//                    userGroup.setGroupName(rs.getString("GROUP_NAME"));
//                    userInfo.setUserGroupInfo(userGroup);
                    userInfo.setBookInfos(getRentBookAlert(connection, type, pid));

                    userInfoList.add(userInfo);
                }
            } catch (Exception e) {
                e.printStackTrace();
                userInfoList = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return userInfoList;
    }

    public List<BookInfo> getRentBookAlert(Connection connection, String type, String uid) {

        BookInfo bookInfo = null;
        List<BookInfo> bookInfos = null;

        UserInfo userInfo = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(AllQueryStatement.selectRentBookAlert(type, uid));
                rs = ps.executeQuery();

                bookInfos = new ArrayList<BookInfo>();

                while (rs.next()) {

                    bookInfo = new BookInfo();

                    userInfo = new UserInfo();

                    SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");

                    bookInfo.setBookId(rs.getString("BOOK_ID"));
                    bookInfo.setBookRef(rs.getString("BOOK_REF"));
                    bookInfo.setBookName(rs.getString("BOOK_NAME"));
                    bookInfo.setBookTitle(rs.getString("BOOK_TITLE"));
                    bookInfo.setBookQuantity(rs.getInt("BOOK_QUANTITY"));
                    bookInfo.setBookRent(rs.getInt("BOOK_RENT_QUANTITY"));
                    bookInfo.setBookCurStock(rs.getInt("BOOK_QUANTITY") - rs.getInt("BOOK_RENT_QUANTITY"));
                    bookInfo.setCategory(rs.getString("BOOK_CATEGORY"));
                    bookInfo.setBookLocation(rs.getString("BOOK_LOCATION"));
                    bookInfo.setDescription(rs.getString("BOOK_DESCRIPTION"));
                    bookInfo.setImgUrl(rs.getString("IMG_URL"));
                    bookInfo.setStatus(rs.getString("BOOK_STATUS").charAt(0));

                    bookInfo.setRentDate(rs.getString("BOOK_RENT_DATE"));
                    bookInfo.setReturnDate(rs.getString("BOOK_RERUTN_DATE"));

                    Calendar returnCal = Calendar.getInstance();
                    returnCal.setTime(rs.getTimestamp("BOOK_RERUTN_DATE"));

                    Calendar rentCal = Calendar.getInstance();
                    rentCal.setTime(rs.getTimestamp("BOOK_RENT_DATE"));

                    Calendar cal = Calendar.getInstance();
                    Date today = cal.getTime();
                    System.out.println("today " + today);

                    long rentDays = (returnCal.getTimeInMillis() - rentCal.getTimeInMillis()) / (1000 * 60 * 60 * 24);
                    bookInfo.setBookRentDays(rentDays);

                    bookInfos.add(bookInfo);
                }
            } catch (Exception e) {
                e.printStackTrace();
                bookInfos = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return bookInfos;
    }

    public static void main(String[] args) {
        Connection c = DBConnection.getMySqlConnection();
        AllList com = new AllList();
        System.out.println(com.getUserInfo(c, "", "", "", "", "100001"));
    }
}
