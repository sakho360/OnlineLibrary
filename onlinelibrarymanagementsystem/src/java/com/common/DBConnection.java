package com.common;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

public class DBConnection {

    private static volatile DBConnection dbConnection = null;
    private static Connection connection = null;
    private static Properties properties = null;
    private static InputStream propertiesFile = null;
    private static String DRIVER = null;
    private static String URL = null;
    private static String USER_NAME = null;
    private static String PASSWORD = null;

    private DBConnection() {
    }

    private static DBConnection getInstance() {

        if (dbConnection == null) {
            synchronized (DBConnection.class) {
                if (dbConnection == null) {
                    dbConnection = new DBConnection();
                }
            }
        }

        return dbConnection;
    }

    public static Connection getMySqlConnection() {

        try {

            properties = new Properties();

            propertiesFile = DBConnection.class.getClassLoader().getResourceAsStream("com/common/db.info.properties");
            properties.load(propertiesFile);

            DRIVER = properties.getProperty("driver");
            URL = properties.getProperty("url");
            USER_NAME = properties.getProperty("username");
            PASSWORD = properties.getProperty("password");

            Class.forName(DRIVER);
            connection = DriverManager.getConnection(URL, USER_NAME, PASSWORD);

        } catch (Exception e) {
            e.printStackTrace();
            connection = null;
        }

        return connection;
    }

    public static void main(String[] args) {
        System.out.println(DBConnection.getMySqlConnection());
    }
}
