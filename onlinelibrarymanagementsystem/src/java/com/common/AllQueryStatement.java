package com.common;

import com.persistance.BookCategoryInfo;
import com.persistance.BookInfo;
import com.persistance.UserInfo;
import java.util.List;

public class AllQueryStatement {

    public static String selectLoginInfo(String userName, String password) {
        String selectLoginStatement = " SELECT "
                + " UI.USER_ID, "
                + " UI.FIRST_NAME, "
                + " UI.LAST_NAME, "
                + " UI.USER_NAME, "
                + " UI.PASSWORD, "
                + " UI.MOBILE, "
                + " UI.USER_IMG, "
                + " UG.GROUP_ID, "
                + " UG.GROUP_NAME "
                + " FROM "
                + " user_info UI, "
                + " user_group UG "
                + " WHERE "
                + " UI.GROUP_ID = UG.GROUP_ID "
                + " AND "
                + " UI.USER_NAME = '" + userName + "' "
                + " AND "
                + " UI.PASSWORD = '" + password + "' "
                + " AND "
                + " UI.STATUS = 'Y' ";

        return selectLoginStatement;
    }

    public static String selectUserInfo(String type, String gid, String id, String userId, String ubrent) {

        String selectUserStatement = "";

        if (type.equals("all")) {
            selectUserStatement = " SELECT "
                    + " UI.USER_ID, "
                    + " UI.FIRST_NAME, "
                    + " UI.LAST_NAME, "
                    + " UI.USER_NAME, "
                    + " UI.DOB, "
                    + " UI.PASSWORD, "
                    + " UI.MOBILE, "
                    + " UI.GENDER, "
                    + " UI.EMAIL, "
                    + " UI.ADDRESS, "
                    + " UI.OCCUPATION, "
                    + " UI.DESCRIPTION, "
                    + " UI.USER_IMG, "
                    + " UI.STATUS, "
                    + " UI.INSERT_DATE, "
                    + " UG.GROUP_ID, "
                    + " UG.GROUP_NAME "
                    + " FROM "
                    + " user_info UI, "
                    + " user_group UG "
                    + " WHERE "
                    + " UI.GROUP_ID = UG.GROUP_ID "
                    + " AND "
                    + " UG.GROUP_ID = '" + gid + "' ";
        } else {
            if (!userId.equals("")) {
                selectUserStatement = " SELECT "
                        + " UI.USER_ID, "
                        + " UI.FIRST_NAME, "
                        + " UI.LAST_NAME, "
                        + " UI.USER_NAME, "
                        + " UI.DOB, "
                        + " UI.PASSWORD, "
                        + " UI.MOBILE, "
                        + " UI.GENDER, "
                        + " UI.EMAIL, "
                        + " UI.ADDRESS, "
                        + " UI.OCCUPATION, "
                        + " UI.DESCRIPTION, "
                        + " UI.USER_IMG, "
                        + " UI.STATUS, "
                        + " UI.INSERT_DATE, "
                        + " UG.GROUP_ID, "
                        + " UG.GROUP_NAME "
                        + " FROM "
                        + " user_info UI, "
                        + " user_group UG "
                        + " WHERE "
                        + " UI.GROUP_ID = UG.GROUP_ID "
                        + " AND "
                        + " UI.USER_ID = '" + userId + "' ";
            } else if (!ubrent.equals("")) {
                selectUserStatement = " SELECT "
                        + " UI.USER_ID, "
                        + " UI.FIRST_NAME, "
                        + " UI.LAST_NAME, "
                        + " UI.USER_NAME, "
                        + " UI.DOB, "
                        + " UI.PASSWORD, "
                        + " UI.MOBILE, "
                        + " UI.GENDER, "
                        + " UI.EMAIL, "
                        + " UI.ADDRESS, "
                        + " UI.OCCUPATION, "
                        + " UI.DESCRIPTION, "
                        + " UI.USER_IMG, "
                        + " UI.STATUS, "
                        + " UI.INSERT_DATE, "
                        + " UG.GROUP_ID, "
                        + " UG.GROUP_NAME "
                        + " FROM "
                        + " user_info UI, "
                        + " user_group UG "
                        + " WHERE "
                        + " UI.GROUP_ID = UG.GROUP_ID "
                        + " AND "
                        + " UI.INSERT_BY = '" + ubrent + "' ";
            } else {
                selectUserStatement = " SELECT "
                        + " UI.USER_ID, "
                        + " UI.FIRST_NAME, "
                        + " UI.LAST_NAME, "
                        + " UI.USER_NAME, "
                        + " UI.DOB, "
                        + " UI.PASSWORD, "
                        + " UI.MOBILE, "
                        + " UI.GENDER, "
                        + " UI.EMAIL, "
                        + " UI.ADDRESS, "
                        + " UI.OCCUPATION, "
                        + " UI.DESCRIPTION, "
                        + " UI.USER_IMG, "
                        + " UI.STATUS, "
                        + " UI.INSERT_DATE, "
                        + " UG.GROUP_ID, "
                        + " UG.GROUP_NAME "
                        + " FROM "
                        + " user_info UI, "
                        + " user_group UG "
                        + " WHERE "
                        + " UI.GROUP_ID = UG.GROUP_ID "
                        + " AND "
                        + " UG.GROUP_ID = '" + gid + "' "
                        + " AND "
                        + " UI.INSERT_BY = '" + id + "' ";
            }
        }

        return selectUserStatement;
    }

    public static String selectUserGoupInfo() {

        String selectUserStatement = "";

        selectUserStatement = " SELECT "
                + " UG.GROUP_ID, "
                + " UG.GROUP_NAME,"
                + " UG.DESCRIPTION, "
                + " UG.STATUS "
                + " FROM "
                + " user_group UG ";

        return selectUserStatement;
    }

    public static String selectSearchUserInfo(String fName, String fValue) {

        String selectAgentStatement = "";

        if (fName.equals("userFulName")) {
            selectAgentStatement = " SELECT "
                    + " UI.USER_ID, "
                    + " UI.USER_FULL_NAME, "
                    + " UI.USER_NAME, "
                    + " UI.USER_PHONE, "
                    + " UI.USER_FAX, "
                    + " UI.USER_EMAIL, "
                    + " UI.USER_DOB, "
                    + " UI.USER_PASSWORD, "
                    + " UI.COMPANY_ID, "
                    + " UI.USER_STATUS, "
                    + " UI.USER_NOTES, "
                    + " UI.USER_ADDRESS, "
                    + " COUN.COUNTRY_ID, "
                    + " COUN.COUNTRY_NAME, "
                    + " UG.GROUP_ID, "
                    + " UG.GROUP_NAME, "
                    + " UG.GROUP_TYPE "
                    + " FROM "
                    + " user_info UI, "
                    + " country_info COUN, "
                    + " user_group UG "
                    + " WHERE "
                    + " UI.COUNTRY_ID = COUN.COUNTRY_ID "
                    + " AND "
                    + " UI.GROUP_ID = UG.GROUP_ID "
                    + " AND "
                    + " UI.USER_FULL_NAME = '" + fValue + "' ";
        } else if (fName.equals("userPhone")) {
            selectAgentStatement = " SELECT "
                    + " UI.USER_ID, "
                    + " UI.USER_FULL_NAME, "
                    + " UI.USER_NAME, "
                    + " UI.USER_PHONE, "
                    + " UI.USER_FAX, "
                    + " UI.USER_EMAIL, "
                    + " UI.USER_DOB, "
                    + " UI.USER_PASSWORD, "
                    + " UI.COMPANY_ID, "
                    + " UI.USER_STATUS, "
                    + " UI.USER_NOTES, "
                    + " UI.USER_ADDRESS, "
                    + " COUN.COUNTRY_ID, "
                    + " COUN.COUNTRY_NAME, "
                    + " UG.GROUP_ID, "
                    + " UG.GROUP_NAME, "
                    + " UG.GROUP_TYPE "
                    + " FROM "
                    + " user_info UI, "
                    + " country_info COUN, "
                    + " user_group UG "
                    + " WHERE "
                    + " UI.COUNTRY_ID = COUN.COUNTRY_ID "
                    + " AND "
                    + " UI.GROUP_ID = UG.GROUP_ID "
                    + " AND "
                    + " UI.USER_PHONE = '" + fValue + "' ";
        } else if (fName.equals("userCompany")) {
            selectAgentStatement = " SELECT "
                    + " UI.USER_ID, "
                    + " UI.USER_FULL_NAME, "
                    + " UI.USER_NAME, "
                    + " UI.USER_PHONE, "
                    + " UI.USER_FAX, "
                    + " UI.USER_EMAIL, "
                    + " UI.USER_DOB, "
                    + " UI.USER_PASSWORD, "
                    + " UI.COMPANY_ID, "
                    + " UI.USER_STATUS, "
                    + " UI.USER_NOTES, "
                    + " UI.USER_ADDRESS, "
                    + " COUN.COUNTRY_ID, "
                    + " COUN.COUNTRY_NAME, "
                    + " UG.GROUP_ID, "
                    + " UG.GROUP_NAME, "
                    + " UG.GROUP_TYPE "
                    + " FROM "
                    + " user_info UI, "
                    + " country_info COUN, "
                    + " user_group UG "
                    + " WHERE "
                    + " UI.COUNTRY_ID = COUN.COUNTRY_ID "
                    + " AND "
                    + " UI.GROUP_ID = UG.GROUP_ID "
                    + " AND "
                    + " UI.COMPANY_ID = '" + fValue + "' ";
        }

        return selectAgentStatement;
    }

    public static String insertBookCategory(List<BookCategoryInfo> bcis) {

        String insertStatement = "";

        for (BookCategoryInfo info : bcis) {

            insertStatement = " INSERT INTO "
                    + " book_category ( "
                    + " BOOK_CATEGORY_NAME, "
                    + " DESCRIPTION, "
                    + " INSERT_BY, "
                    + " INSERT_DATE "
                    + " )VALUES( "
                    + " '" + info.getBookCategoryName() + "', "
                    + " '" + info.getCategoryDescription() + "', "
                    + " '" + info.getInsertBy() + "', "
                    + " CURRENT_TIMESTAMP)";
        }

        return insertStatement;
    }

    public static String selectBookCategoryInfo(String type, String id, Integer bcid) {

        String selectBookCategoryStatement = "";

        if (type.equals("all")) {
            selectBookCategoryStatement = " SELECT "
                    + " BCI.BOOK_CATEGORY_ID, "
                    + " BCI.BOOK_CATEGORY_NAME, "
                    + " BCI.DESCRIPTION, "
                    + " BCI.STATUS, "
                    + " BCI.INSERT_BY, "
                    + " BCI.INSERT_DATE, "
                    + " UI.USER_ID, "
                    + " UI.FIRST_NAME, "
                    + " UI.LAST_NAME "
                    + " FROM "
                    + " book_category BCI, "
                    + " user_info UI "
                    + " WHERE "
                    + " BCI.INSERT_BY  = UI.USER_ID ";
        } else {
            if (bcid != 0) {
                selectBookCategoryStatement = " SELECT "
                        + " BCI.BOOK_CATEGORY_ID, "
                        + " BCI.BOOK_CATEGORY_NAME, "
                        + " BCI.DESCRIPTION, "
                        + " BCI.STATUS, "
                        + " BCI.INSERT_BY, "
                        + " BCI.INSERT_DATE, "
                        + " UI.USER_ID, "
                        + " UI.FIRST_NAME, "
                        + " UI.LAST_NAME "
                        + " FROM "
                        + " book_category BCI, "
                        + " user_info UI "
                        + " WHERE "
                        + " BCI.INSERT_BY  = UI.USER_ID "
                        + " AND "
                        + " BCI.BOOK_CATEGORY_ID = '" + bcid + "' ";
            } else {
                selectBookCategoryStatement = " SELECT "
                        + " BCI.BOOK_CATEGORY_ID, "
                        + " BCI.BOOK_CATEGORY_NAME, "
                        + " BCI.DESCRIPTION, "
                        + " BCI.STATUS, "
                        + " BCI.INSERT_BY, "
                        + " BCI.INSERT_DATE, "
                        + " UI.USER_ID, "
                        + " UI.FIRST_NAME, "
                        + " UI.LAST_NAME "
                        + " FROM "
                        + " book_category BCI, "
                        + " user_info UI "
                        + " WHERE "
                        + " BCI.INSERT_BY  = UI.USER_ID "
                        + " AND "
                        + " UI.USER_ID = '" + id + "' ";
            }
        }

        return selectBookCategoryStatement;
    }

    public static String insertBook(List<BookInfo> bookInfos) {

        String insertStatement = "";

        for (BookInfo info : bookInfos) {

            insertStatement = " INSERT INTO "
                    + " book_info ( "
                    + " BOOK_ID, "
                    + " BOOK_REF, "
                    + " BOOK_NAME, "
                    + " BOOK_TITLE, "
                    + " BOOK_QUANTITY, "
                    + " BOOK_LOCATION, "
                    + " BOOK_DESCRIPTION, "
                    + " BOOK_CATEGORY, "
                    + " IMG_URL, "
                    + " INSERT_BY, "
                    + " INSERT_DATE "
                    + " )VALUES( "
                    + " '" + info.getBookId() + "', "
                    + " '" + info.getBookRef() + "', "
                    + " '" + info.getBookName() + "', "
                    + " '" + info.getBookTitle() + "', "
                    + " '" + info.getBookQuantity() + "', "
                    + " '" + info.getBookLocation() + "', "
                    + " '" + info.getDescription() + "', "
                    + " '" + info.getCategory() + "', "
                    + " '" + info.getImgUrl() + "', "
                    + " '" + info.getInsertBy() + "', "
                    + " CURRENT_TIMESTAMP)";
        }

        return insertStatement;
    }

    public static String selectBookInfo(String type, String id, String bookid) {

        String selectBookStatement = "";

        if (type.equals("all")) {
            selectBookStatement = " SELECT "
                    + " BI.BOOK_ID, "
                    + " BI.BOOK_REF, "
                    + " BI.BOOK_NAME, "
                    + " BI.BOOK_TITLE, "
                    + " BI.BOOK_QUANTITY, "
                    + " BI.BOOK_LOCATION, "
                    + " BI.BOOK_DESCRIPTION, "
                    + " BI.BOOK_CATEGORY, "
                    + " BI.IMG_URL, "
                    + " BI.BOOK_STATUS, "
                    + " BI.INSERT_DATE, "
                    + " SUM(UBM.BOOK_RENT_QUANTITY) AS RENT "
                    + " FROM "
                    + " book_info BI "
                    + " LEFT JOIN (user_book_map UBM) "
                    + " ON "
                    + " BI.BOOK_ID = UBM.BOOK_ID "
                    + " GROUP BY  "
                    + " BI.BOOK_ID ";
        } else {
            if (!bookid.equals("")) {
                selectBookStatement = " SELECT "
                        + " BI.BOOK_ID, "
                        + " BI.BOOK_REF, "
                        + " BI.BOOK_NAME, "
                        + " BI.BOOK_TITLE, "
                        + " BI.BOOK_QUANTITY, "
                        + " BI.BOOK_LOCATION, "
                        + " BI.BOOK_DESCRIPTION, "
                        + " BI.BOOK_CATEGORY, "
                        + " BI.IMG_URL, "
                        + " BI.BOOK_STATUS, "
                        + " BI.INSERT_DATE, "
                        + " SUM(UBM.BOOK_RENT_QUANTITY) AS RENT "
                        + " FROM "
                        + " book_info BI "
                        + " LEFT JOIN (user_book_map UBM) "
                        + " ON "
                        + " BI.BOOK_ID = UBM.BOOK_ID "
                        + " WHERE "
                        + " BI.BOOK_ID = '" + bookid + "' "
                        + " GROUP BY  "
                        + " BI.BOOK_ID ";
            } else {
                selectBookStatement = " SELECT "
                        + " BI.BOOK_ID, "
                        + " BI.BOOK_REF, "
                        + " BI.BOOK_NAME, "
                        + " BI.BOOK_TITLE, "
                        + " BI.BOOK_QUANTITY, "
                        + " BI.BOOK_LOCATION, "
                        + " BI.BOOK_DESCRIPTION, "
                        + " BI.BOOK_CATEGORY, "
                        + " BI.IMG_URL, "
                        + " BI.BOOK_STATUS, "
                        + " BI.INSERT_DATE, "
                        + " SUM(UBM.BOOK_RENT_QUANTITY) AS RENT "
                        + " FROM "
                        + " book_info BI "
                        + " LEFT JOIN (user_book_map UBM) "
                        + " ON "
                        + " BI.BOOK_ID = UBM.BOOK_ID "
                        + " WHERE "
                        + " BI.INSERT_BY = '" + id + "' "
                        + " GROUP BY  "
                        + " BI.BOOK_ID ";
            }
        }

        return selectBookStatement;
    }

    public static String insertUser(List<UserInfo> userInfos) {

        String insertStatement = "";

        for (UserInfo info : userInfos) {

            insertStatement = " INSERT INTO "
                    + " user_info ( "
                    + " USER_ID, "
                    + " FIRST_NAME, "
                    + " LAST_NAME, "
                    + " USER_NAME, "
                    + " DOB, "
                    + " PASSWORD, "
                    + " MOBILE, "
                    + " GENDER, "
                    + " EMAIL, "
                    + " ADDRESS, "
                    + " OCCUPATION, "
                    + " DESCRIPTION, "
                    + " USER_IMG, "
                    + " GROUP_ID, "
                    + " INSERT_BY, "
                    + " INSERT_DATE "
                    + " )VALUES( "
                    + " '" + info.getUserid() + "', "
                    + " '" + info.getFirstName() + "', "
                    + " '" + info.getLastName() + "', "
                    + " '" + info.getUserName() + "', "
                    + " '" + info.getUserDob() + "', "
                    + " '" + info.getUserPassword() + "', "
                    + " '" + info.getUserMobile() + "', "
                    + " '" + info.getGender() + "', "
                    + " '" + info.getUserEmail() + "', "
                    + " '" + info.getUserAddress() + "', "
                    + " '" + info.getOccupation() + "', "
                    + " '" + info.getDescription() + "', "
                    + " '" + info.getUserImage() + "', "
                    + " '" + info.getUserGroupInfo().getUserGroupID() + "', "
                    + " '" + info.getInsertBy() + "', "
                    + " CURRENT_TIMESTAMP)";
        }

        return insertStatement;
    }

    public static String insertBookRent(List<BookInfo> bookInfos) {

        String insertStatement = "";

        for (BookInfo info : bookInfos) {

            insertStatement = " INSERT INTO "
                    + " user_book_map ( "
                    + " USER_ID, "
                    + " BOOK_ID, "
                    + " BOOK_RENT_QUANTITY, "
                    + " BOOK_RENT_DATE, "
                    + " BOOK_RERUTN_DATE, "
                    + " INSERT_BY, "
                    + " INSERT_DATE "
                    + " )VALUES( "
                    + " '" + info.getUserInfo().getUserid() + "', "
                    + " '" + info.getBookId() + "', "
                    + " '" + info.getBookQuantity() + "', "
                    + " '" + info.getRentDate() + "', "
                    + " '" + info.getReturnDate() + "', "
                    + " '" + info.getInsertBy() + "', "
                    + " CURRENT_TIMESTAMP)";
        }

        return insertStatement;
    }

    public static String selectBookRentByUser(String type, String id, String uid) {

        String selectUserBookStatement = "";

        if (type.equals("all")) {
            selectUserBookStatement = " SELECT "
                    + " UBM.USER_BOOK_ID, "
                    + " UI.USER_ID, "
                    + " UI.FIRST_NAME, "
                    + " UI.LAST_NAME, "
                    + " UI.USER_NAME, "
                    + " UI.DOB, "
                    + " UI.PASSWORD, "
                    + " UI.MOBILE, "
                    + " UI.GENDER, "
                    + " UI.EMAIL, "
                    + " UI.ADDRESS, "
                    + " UI.OCCUPATION, "
                    + " UI.DESCRIPTION, "
                    + " UI.USER_IMG, "
                    + " UI.STATUS, "
                    + " UI.INSERT_DATE "
                    + " FROM "
                    + " user_book_map UBM "
                    + " LEFT JOIN(user_info UI) "
                    + " ON "
                    + " UBM.USER_ID = UI.USER_ID "
                    + " GROUP BY "
                    + " UBM.USER_ID ";
        } else {
            if (!id.equals("")) {
                selectUserBookStatement = " SELECT "
                        + " UBM.USER_BOOK_ID, "
                        + " UI.USER_ID, "
                        + " UI.FIRST_NAME, "
                        + " UI.LAST_NAME, "
                        + " UI.USER_NAME, "
                        + " UI.DOB, "
                        + " UI.PASSWORD, "
                        + " UI.MOBILE, "
                        + " UI.GENDER, "
                        + " UI.EMAIL, "
                        + " UI.ADDRESS, "
                        + " UI.OCCUPATION, "
                        + " UI.DESCRIPTION, "
                        + " UI.USER_IMG, "
                        + " UI.STATUS, "
                        + " UI.INSERT_DATE "
                        + " FROM "
                        + " user_book_map UBM "
                        + " LEFT JOIN(user_info UI) "
                        + " ON "
                        + " UBM.USER_ID = UI.USER_ID "
                        + " WHERE "
                        + " UBM.INSERT_BY = '" + id + "' "
                        + " GROUP BY "
                        + " UBM.USER_ID ";

            } else {
                selectUserBookStatement = " SELECT "
                        + " UBM.USER_BOOK_ID, "
                        + " UI.USER_ID, "
                        + " UI.FIRST_NAME, "
                        + " UI.LAST_NAME, "
                        + " UI.USER_NAME, "
                        + " UI.DOB, "
                        + " UI.PASSWORD, "
                        + " UI.MOBILE, "
                        + " UI.GENDER, "
                        + " UI.EMAIL, "
                        + " UI.ADDRESS, "
                        + " UI.OCCUPATION, "
                        + " UI.DESCRIPTION, "
                        + " UI.USER_IMG, "
                        + " UI.STATUS, "
                        + " UI.INSERT_DATE "
                        + " FROM "
                        + " user_book_map UBM "
                        + " LEFT JOIN(user_info UI) "
                        + " ON "
                        + " UBM.USER_ID = UI.USER_ID "
                        + " WHERE "
                        + " UBM.USER_ID = '" + uid + "' "
                        + " GROUP BY "
                        + " UBM.USER_ID ";
            }
        }

        return selectUserBookStatement;
    }

    public static String selectRentBookInfo(String id) {

        String selectBookStatement = "";

        selectBookStatement = " SELECT "
                + " BI.BOOK_ID, "
                + " BI.BOOK_REF, "
                + " BI.BOOK_NAME, "
                + " BI.BOOK_TITLE, "
                + " BI.BOOK_QUANTITY, "
                + " BI.BOOK_CATEGORY, "
                + " BI.BOOK_LOCATION, "
                + " BI.BOOK_DESCRIPTION, "
                + " BI.IMG_URL, "
                + " BI.BOOK_STATUS, "
                + " BI.INSERT_DATE, "
                + " UBM.BOOK_RENT_QUANTITY, "
                + " UBM.BOOK_RENT_DATE, "
                + " UBM.BOOK_RERUTN_DATE "
                + " FROM "
                + " book_info BI, "
                + " user_book_map UBM "
                + " WHERE "
                + " BI.BOOK_ID = UBM.BOOK_ID "
                + " AND "
                + " UBM.USER_ID = '" + id + "' ";

        return selectBookStatement;
    }

    public static String selectUserBookRentAlert(String type, String pid) {

        String selectUserBookStatement = "";

        if (type.equals("all")) {
            selectUserBookStatement = " SELECT "
                    + " UBM.USER_BOOK_ID, "
                    + " UI.USER_ID, "
                    + " UI.FIRST_NAME, "
                    + " UI.LAST_NAME, "
                    + " UI.USER_NAME, "
                    + " UI.DOB, "
                    + " UI.PASSWORD, "
                    + " UI.MOBILE, "
                    + " UI.GENDER, "
                    + " UI.EMAIL, "
                    + " UI.ADDRESS, "
                    + " UI.OCCUPATION, "
                    + " UI.DESCRIPTION, "
                    + " UI.USER_IMG, "
                    + " UI.STATUS, "
                    + " UI.INSERT_DATE "
                    + " FROM "
                    + " user_book_map UBM "
                    + " LEFT JOIN(user_info UI) "
                    + " ON "
                    + " UBM.USER_ID = UI.USER_ID "
                    + " GROUP BY "
                    + " UBM.USER_ID ";
        } else {
            selectUserBookStatement = " SELECT "
                    + " UBM.USER_BOOK_ID, "
                    + " UI.USER_ID, "
                    + " UI.FIRST_NAME, "
                    + " UI.LAST_NAME, "
                    + " UI.USER_NAME, "
                    + " UI.DOB, "
                    + " UI.PASSWORD, "
                    + " UI.MOBILE, "
                    + " UI.GENDER, "
                    + " UI.EMAIL, "
                    + " UI.ADDRESS, "
                    + " UI.OCCUPATION, "
                    + " UI.DESCRIPTION, "
                    + " UI.USER_IMG, "
                    + " UI.STATUS, "
                    + " UI.INSERT_DATE "
                    + " FROM "
                    + " user_book_map UBM "
                    + " LEFT JOIN(user_info UI) "
                    + " ON "
                    + " UBM.USER_ID = UI.USER_ID "
                    + " WHERE "
                    + " UBM.INSERT_BY = '" + pid + "' "
                    + " GROUP BY "
                    + " UBM.USER_ID ";
        }

        return selectUserBookStatement;
    }

    public static String selectRentBookAlert(String type, String id) {

        String selectBookStatement = "";

        if (type.equals("all")) {
            selectBookStatement = " SELECT "
                    + " BI.BOOK_ID, "
                    + " BI.BOOK_REF, "
                    + " BI.BOOK_NAME, "
                    + " BI.BOOK_TITLE, "
                    + " BI.BOOK_QUANTITY, "
                    + " BI.BOOK_CATEGORY, "
                    + " BI.BOOK_LOCATION, "
                    + " BI.BOOK_DESCRIPTION, "
                    + " BI.IMG_URL, "
                    + " BI.BOOK_STATUS, "
                    + " BI.INSERT_DATE, "
                    + " UBM.BOOK_RENT_QUANTITY, "
                    + " UBM.BOOK_RENT_DATE, "
                    + " UBM.BOOK_RERUTN_DATE "
                    + " FROM "
                    + " book_info BI, "
                    + " user_book_map UBM "
                    + " WHERE "
                    + " BI.BOOK_ID = UBM.BOOK_ID "
                    + " AND "
                    + " DATE(UBM.BOOK_RERUTN_DATE) < NOW()";
        } else {
            selectBookStatement = " SELECT "
                    + " BI.BOOK_ID, "
                    + " BI.BOOK_REF, "
                    + " BI.BOOK_NAME, "
                    + " BI.BOOK_TITLE, "
                    + " BI.BOOK_QUANTITY, "
                    + " BI.BOOK_CATEGORY, "
                    + " BI.BOOK_LOCATION, "
                    + " BI.BOOK_DESCRIPTION, "
                    + " BI.IMG_URL, "
                    + " BI.BOOK_STATUS, "
                    + " BI.INSERT_DATE, "
                    + " UBM.BOOK_RENT_QUANTITY, "
                    + " UBM.BOOK_RENT_DATE, "
                    + " UBM.BOOK_RERUTN_DATE "
                    + " FROM "
                    + " book_info BI, "
                    + " user_book_map UBM "
                    + " WHERE "
                    + " BI.BOOK_ID = UBM.BOOK_ID "
                    + " AND "
                    + " DATE(UBM.BOOK_RERUTN_DATE) < NOW()"
                    + " AND "
                    + " UBM.INSERT_BY = '" + id + "' ";
        }

        return selectBookStatement;
    }
}
