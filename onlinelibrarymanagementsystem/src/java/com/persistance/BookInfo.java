package com.persistance;

public class BookInfo {

    private String bookId;
    private String bookRef;
    private String bookName;
    private String bookTitle;
    private Integer bookQuantity;
    private Integer bookRent;
    private Integer bookCurStock;
    private String bookLocation;
    private String description;
    private String imgUrl;
    private String category;
    private Character status;
    private String insertBy;
    private String insertDate;
    private String updateBy;
    private String updateDate;
    private String deleteBy;
    private String deleteDate;
    private String rentDate;
    private String returnDate;
    private Long bookRentDays;
    private UserInfo userInfo;

    /**
     * @return the bookId
     */
    public String getBookId() {
        return bookId;
    }

    /**
     * @param bookId the bookId to set
     */
    public void setBookId(String bookId) {
        this.bookId = bookId;
    }

    /**
     * @return the bookRef
     */
    public String getBookRef() {
        return bookRef;
    }

    /**
     * @param bookRef the bookRef to set
     */
    public void setBookRef(String bookRef) {
        this.bookRef = bookRef;
    }

    /**
     * @return the bookName
     */
    public String getBookName() {
        return bookName;
    }

    /**
     * @param bookName the bookName to set
     */
    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    /**
     * @return the bookTitle
     */
    public String getBookTitle() {
        return bookTitle;
    }

    /**
     * @param bookTitle the bookTitle to set
     */
    public void setBookTitle(String bookTitle) {
        this.bookTitle = bookTitle;
    }

    /**
     * @return the bookQuantity
     */
    public Integer getBookQuantity() {
        return bookQuantity;
    }

    /**
     * @param bookQuantity the bookQuantity to set
     */
    public void setBookQuantity(Integer bookQuantity) {
        this.bookQuantity = bookQuantity;
    }

    /**
     * @return the bookRent
     */
    public Integer getBookRent() {
        return bookRent;
    }

    /**
     * @param bookRent the bookRent to set
     */
    public void setBookRent(Integer bookRent) {
        this.bookRent = bookRent;
    }

    /**
     * @return the bookCurStock
     */
    public Integer getBookCurStock() {
        return bookCurStock;
    }

    /**
     * @param bookCurStock the bookCurStock to set
     */
    public void setBookCurStock(Integer bookCurStock) {
        this.bookCurStock = bookCurStock;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the category
     */
    public String getCategory() {
        return category;
    }

    /**
     * @param category the category to set
     */
    public void setCategory(String category) {
        this.category = category;
    }

    /**
     * @return the status
     */
    public Character getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(Character status) {
        this.status = status;
    }

    /**
     * @return the insertBy
     */
    public String getInsertBy() {
        return insertBy;
    }

    /**
     * @param insertBy the insertBy to set
     */
    public void setInsertBy(String insertBy) {
        this.insertBy = insertBy;
    }

    /**
     * @return the insertDate
     */
    public String getInsertDate() {
        return insertDate;
    }

    /**
     * @param insertDate the insertDate to set
     */
    public void setInsertDate(String insertDate) {
        this.insertDate = insertDate;
    }

    /**
     * @return the updateBy
     */
    public String getUpdateBy() {
        return updateBy;
    }

    /**
     * @param updateBy the updateBy to set
     */
    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    /**
     * @return the updateDate
     */
    public String getUpdateDate() {
        return updateDate;
    }

    /**
     * @param updateDate the updateDate to set
     */
    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    /**
     * @return the deleteBy
     */
    public String getDeleteBy() {
        return deleteBy;
    }

    /**
     * @param deleteBy the deleteBy to set
     */
    public void setDeleteBy(String deleteBy) {
        this.deleteBy = deleteBy;
    }

    /**
     * @return the deleteDate
     */
    public String getDeleteDate() {
        return deleteDate;
    }

    /**
     * @param deleteDate the deleteDate to set
     */
    public void setDeleteDate(String deleteDate) {
        this.deleteDate = deleteDate;
    }

    /**
     * @return the bookLocation
     */
    public String getBookLocation() {
        return bookLocation;
    }

    /**
     * @param bookLocation the bookLocation to set
     */
    public void setBookLocation(String bookLocation) {
        this.bookLocation = bookLocation;
    }

    /**
     * @return the imgUrl
     */
    public String getImgUrl() {
        return imgUrl;
    }

    /**
     * @param imgUrl the imgUrl to set
     */
    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    /**
     * @return the rentDate
     */
    public String getRentDate() {
        return rentDate;
    }

    /**
     * @param rentDate the rentDate to set
     */
    public void setRentDate(String rentDate) {
        this.rentDate = rentDate;
    }

    /**
     * @return the returnDate
     */
    public String getReturnDate() {
        return returnDate;
    }

    /**
     * @param returnDate the returnDate to set
     */
    public void setReturnDate(String returnDate) {
        this.returnDate = returnDate;
    }

    /**
     * @return the userInfo
     */
    public UserInfo getUserInfo() {
        return userInfo;
    }

    /**
     * @param userInfo the userInfo to set
     */
    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    /**
     * @return the bookRentDays
     */
    public Long getBookRentDays() {
        return bookRentDays;
    }

    /**
     * @param bookRentDays the bookRentDays to set
     */
    public void setBookRentDays(Long bookRentDays) {
        this.bookRentDays = bookRentDays;
    }
}
