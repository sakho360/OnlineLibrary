package com.persistance;

public class MenuChild {

    private Integer childMenuID;
    private String childMenuName;
    private String childMenuTitle;
    private Integer childParentMenuID;
    private Character childMenuStatus;
    private String childMenuInsertBy;
    private String childMenuInsertDate;
    private String childMenuUpdateBy;
    private String childMenuUpdateDate;
    private UserLevelMenu userLevelMenu;

    /**
     * @return the childMenuID
     */
    public Integer getChildMenuID() {
        return childMenuID;
    }

    /**
     * @param childMenuID the childMenuID to set
     */
    public void setChildMenuID(Integer childMenuID) {
        this.childMenuID = childMenuID;
    }

    /**
     * @return the childMenuName
     */
    public String getChildMenuName() {
        return childMenuName;
    }

    /**
     * @param childMenuName the childMenuName to set
     */
    public void setChildMenuName(String childMenuName) {
        this.childMenuName = childMenuName;
    }

    /**
     * @return the childMenuTitle
     */
    public String getChildMenuTitle() {
        return childMenuTitle;
    }

    /**
     * @param childMenuTitle the childMenuTitle to set
     */
    public void setChildMenuTitle(String childMenuTitle) {
        this.childMenuTitle = childMenuTitle;
    }

    /**
     * @return the childParentMenuID
     */
    public Integer getChildParentMenuID() {
        return childParentMenuID;
    }

    /**
     * @param childParentMenuID the childParentMenuID to set
     */
    public void setChildParentMenuID(Integer childParentMenuID) {
        this.childParentMenuID = childParentMenuID;
    }

    /**
     * @return the childMenuStatus
     */
    public Character getChildMenuStatus() {
        return childMenuStatus;
    }

    /**
     * @param childMenuStatus the childMenuStatus to set
     */
    public void setChildMenuStatus(Character childMenuStatus) {
        this.childMenuStatus = childMenuStatus;
    }

    /**
     * @return the childMenuInsertBy
     */
    public String getChildMenuInsertBy() {
        return childMenuInsertBy;
    }

    /**
     * @param childMenuInsertBy the childMenuInsertBy to set
     */
    public void setChildMenuInsertBy(String childMenuInsertBy) {
        this.childMenuInsertBy = childMenuInsertBy;
    }

    /**
     * @return the childMenuInsertDate
     */
    public String getChildMenuInsertDate() {
        return childMenuInsertDate;
    }

    /**
     * @param childMenuInsertDate the childMenuInsertDate to set
     */
    public void setChildMenuInsertDate(String childMenuInsertDate) {
        this.childMenuInsertDate = childMenuInsertDate;
    }

    /**
     * @return the childMenuUpdateBy
     */
    public String getChildMenuUpdateBy() {
        return childMenuUpdateBy;
    }

    /**
     * @param childMenuUpdateBy the childMenuUpdateBy to set
     */
    public void setChildMenuUpdateBy(String childMenuUpdateBy) {
        this.childMenuUpdateBy = childMenuUpdateBy;
    }

    /**
     * @return the childMenuUpdateDate
     */
    public String getChildMenuUpdateDate() {
        return childMenuUpdateDate;
    }

    /**
     * @param childMenuUpdateDate the childMenuUpdateDate to set
     */
    public void setChildMenuUpdateDate(String childMenuUpdateDate) {
        this.childMenuUpdateDate = childMenuUpdateDate;
    }

    /**
     * @return the userLevelMenu
     */
    public UserLevelMenu getUserLevelMenu() {
        return userLevelMenu;
    }

    /**
     * @param userLevelMenu the userLevelMenu to set
     */
    public void setUserLevelMenu(UserLevelMenu userLevelMenu) {
        this.userLevelMenu = userLevelMenu;
    }
}
