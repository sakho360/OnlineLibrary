package com.persistance;

import java.util.List;

public class MenuMaster {

    private Integer masterMenuID;
    private String masterMenuName;
    private String masterMenuTitle;
    private Integer masterParentMenuID;
    private Character masterMenuStatus;
    private String masterMenuInsertBy;
    private String masterMenuInsertDate;
    private String masterMenuUpdateBy;
    private String masterMenuUpdateDate;
    private List<MenuChild> menuChildList;

    /**
     * @return the masterMenuID
     */
    public Integer getMasterMenuID() {
        return masterMenuID;
    }

    /**
     * @param masterMenuID the masterMenuID to set
     */
    public void setMasterMenuID(Integer masterMenuID) {
        this.masterMenuID = masterMenuID;
    }

    /**
     * @return the masterMenuName
     */
    public String getMasterMenuName() {
        return masterMenuName;
    }

    /**
     * @param masterMenuName the masterMenuName to set
     */
    public void setMasterMenuName(String masterMenuName) {
        this.masterMenuName = masterMenuName;
    }

    /**
     * @return the masterMenuTitle
     */
    public String getMasterMenuTitle() {
        return masterMenuTitle;
    }

    /**
     * @param masterMenuTitle the masterMenuTitle to set
     */
    public void setMasterMenuTitle(String masterMenuTitle) {
        this.masterMenuTitle = masterMenuTitle;
    }

    /**
     * @return the masterParentMenuID
     */
    public Integer getMasterParentMenuID() {
        return masterParentMenuID;
    }

    /**
     * @param masterParentMenuID the masterParentMenuID to set
     */
    public void setMasterParentMenuID(Integer masterParentMenuID) {
        this.masterParentMenuID = masterParentMenuID;
    }

    /**
     * @return the masterMenuStatus
     */
    public Character getMasterMenuStatus() {
        return masterMenuStatus;
    }

    /**
     * @param masterMenuStatus the masterMenuStatus to set
     */
    public void setMasterMenuStatus(Character masterMenuStatus) {
        this.masterMenuStatus = masterMenuStatus;
    }

    /**
     * @return the masterMenuInsertBy
     */
    public String getMasterMenuInsertBy() {
        return masterMenuInsertBy;
    }

    /**
     * @param masterMenuInsertBy the masterMenuInsertBy to set
     */
    public void setMasterMenuInsertBy(String masterMenuInsertBy) {
        this.masterMenuInsertBy = masterMenuInsertBy;
    }

    /**
     * @return the masterMenuInsertDate
     */
    public String getMasterMenuInsertDate() {
        return masterMenuInsertDate;
    }

    /**
     * @param masterMenuInsertDate the masterMenuInsertDate to set
     */
    public void setMasterMenuInsertDate(String masterMenuInsertDate) {
        this.masterMenuInsertDate = masterMenuInsertDate;
    }

    /**
     * @return the masterMenuUpdateBy
     */
    public String getMasterMenuUpdateBy() {
        return masterMenuUpdateBy;
    }

    /**
     * @param masterMenuUpdateBy the masterMenuUpdateBy to set
     */
    public void setMasterMenuUpdateBy(String masterMenuUpdateBy) {
        this.masterMenuUpdateBy = masterMenuUpdateBy;
    }

    /**
     * @return the masterMenuUpdateDate
     */
    public String getMasterMenuUpdateDate() {
        return masterMenuUpdateDate;
    }

    /**
     * @param masterMenuUpdateDate the masterMenuUpdateDate to set
     */
    public void setMasterMenuUpdateDate(String masterMenuUpdateDate) {
        this.masterMenuUpdateDate = masterMenuUpdateDate;
    }

    /**
     * @return the menuChildList
     */
    public List<MenuChild> getMenuChildList() {
        return menuChildList;
    }

    /**
     * @param menuChildList the menuChildList to set
     */
    public void setMenuChildList(List<MenuChild> menuChildList) {
        this.menuChildList = menuChildList;
    }
}
