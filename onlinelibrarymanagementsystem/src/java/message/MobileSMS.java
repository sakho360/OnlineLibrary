package message;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Date;
import org.apache.tomcat.util.codec.binary.Base64;

public class MobileSMS {

    public static void main(String[] args) {

    }

    public static void senssms1() {

        // Declare the security credentials to use
        String username = "my_username";
        String password = "SecrEt12345";

        // Set the attributes of the message to send
        String message = "Hello World";
        String type = "1-way";
        String senderid = "XYZCorp";
        String to = "+8801834623824";

        try {

            // Build URL encoded query string
            String encoding = "UTF-8";
            String queryString = "username=" + URLEncoder.encode(username, encoding)
                    + "&password=" + URLEncoder.encode(password, encoding)
                    + "&message=" + URLEncoder.encode(message, encoding)
                    + "&senderid=" + URLEncoder.encode(senderid, encoding)
                    + "&to=" + URLEncoder.encode(to, encoding)
                    + "&type=" + URLEncoder.encode(type, encoding);

            // Send request to the API servers over HTTPS
            URL url = new URL("https://api.directsms.com.au/s3/http/send_message?");
            URLConnection conn = url.openConnection();
            conn.setDoOutput(true);
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write(queryString);
            wr.flush();

            // The response from the gateway is going to look like this:
            // id: a4c5ad77ad6faf5aa55f66a
            // 
            // In the event of an error, it will look like this:
            // err: invalid login credentials
            BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String result = rd.readLine();
            wr.close();
            rd.close();

            if (result == null) {
                System.out.println("No response received");
            } else if (result.startsWith("id:")) {
                System.out.println("Message sent successfully");
            } else {
                System.out.println("Error - " + result);
            }
        } catch (Exception e) {
            System.out.println("Error - " + e);
        }
    }

    public static void senssms() {
        try {
            String phoneNumber = "01913912448";
            String appKey = "9988774455";
            String appSecret = "your-app-secret";
            String message = "Hello, world!";

            URL url = new URL("https://messagingapi.sinch.com/v1/sms/" + phoneNumber);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestMethod("POST");

            connection.setRequestProperty("Content-Type", "application/json");
            String userCredentials = "application\\" + appKey + ":" + appSecret;
            byte[] encoded = Base64.encodeBase64(userCredentials.getBytes());
            String basicAuth = "Basic " + new String(encoded);
            connection.setRequestProperty("Authorization", basicAuth);
            String postData = "{\"Message\":\"" + message + "\"}";

            OutputStream os = connection.getOutputStream();
            os.write(postData.getBytes());
            StringBuilder response = new StringBuilder();
            BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));

            String line;
            while ((line = br.readLine()) != null) {
                response.append(line);
            }

            br.close();
            os.close();

            System.out.println(response.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void sms() {
        try {

            Date mydate = new Date(System.currentTimeMillis());
            String data = "";
            data += "Type=Individual"; // can be Bulk/Group
            data += "&UserName=YourUsername"; // your Username
            data += "&Password=" + URLEncoder.encode("xxxxxx", "UTF-8"); // your password
            data += "&Message=" + URLEncoder.encode("SMS Gateway message " + mydate.toString(), "UTF - 8 ");
            data += "&To=" + URLEncoder.encode("9xxxxxxxxx", "UTF-8"); // a valid 10 digit phone no.
            data += "&Mask=" + URLEncoder.encode("SenderName", "UTF-8"); // your approved sender name.
            data += "&Language=English"; // Can be "OTHER" for UNICODE

            URL url = new URL("https://www.smsgatewaycenter.com/library/send_sms_2.php?" + data);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setUseCaches(false);
            conn.connect();

            BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;
            StringBuffer buffer = new StringBuffer();

            while ((line = rd.readLine()) != null) {
                buffer.append(line).append("\n");
            }

            System.out.println(buffer.toString());

            rd.close();
            conn.disconnect();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
