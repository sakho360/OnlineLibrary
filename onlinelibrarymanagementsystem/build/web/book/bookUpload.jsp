<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="/start/header_resources.jsp" %>

        <!-- page specific plugin styles -->
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath()%>/css/jquery-ui.custom.min.css" />
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath()%>/css/chosen.min.css" />
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath()%>/css/bootstrap-datepicker3.min.css" />
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath()%>/css/bootstrap-timepicker.min.css" />
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath()%>/css/daterangepicker.min.css" />
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath()%>/css/bootstrap-datetimepicker.min.css" />
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath()%>/css/bootstrap-colorpicker.min.css" />

        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath()%>/css/bootstrap-multiselect.min.css" />
    </head>
    <body class="no-skin">

        <%@include file="/start/top_bar.jsp" %>

        <div class="main-container ace-save-state" id="main-container">

            <%@include file="/start/left_manu.jsp" %>

            <div class="main-content">
                <div class="main-content-inner">
                    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
                        <ul class="breadcrumb">
                            <li>
                                <i class="ace-icon fa fa-home home-icon"></i>
                                <a href="Dashboard">Home</a>
                            </li>

                            <li>
                                <a href="UploadBook">Book</a>
                            </li>
                            <li class="active">Upload Book</li>
                        </ul>
                        <!-- /.breadcrumb -->

                        <div class="nav-search" id="nav-search">
                            <form class="form-search">
                                <span class="input-icon">
                                    <input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
                                    <i class="ace-icon fa fa-search nav-search-icon"></i>
                                </span>
                            </form>
                        </div>
                        <!-- /.nav-search -->
                    </div>

                    <div class="page-content">

                        <%@include file="/start/settings_box.jsp" %>

                        <div class="page-header">
                            <h1>
                                Upload Book
                                <small>
                                    <i class="ace-icon fa fa-angle-double-right"></i>
                                    Set Details of Book
                                </small>
                            </h1>
                        </div>
                        <!-- /.page-header -->

                        <div class="row">
                            <div class="col-xs-12">
                                <!-- PAGE CONTENT BEGINS -->
                                <form action="AddNewBook" method="post" class="form-horizontal" role="form" enctype="multipart/form-data">

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="bookReference">Book Reference</label>
                                        <div class="col-sm-9">
                                            <input type="text" id="bookReference" name="bookReference" pattern="[a-zA-Z0-9-. ]{3,20}" required placeholder="Book Reference Code" class="col-xs-10 col-sm-5">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="bookName">Book Name</label>
                                        <div class="col-sm-9">
                                            <input type="text" id="bookName" name="bookName" pattern="[a-zA-Z0-9-. ]{3,20}" required placeholder="Book Name" class="col-xs-10 col-sm-5">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="bookTitle">Book Title</label>
                                        <div class="col-sm-9">
                                            <input type="text" id="bookTitle" name="bookTitle" pattern="[a-zA-Z0-9-. ]{3,20}" required placeholder="Book Title" class="col-xs-10 col-sm-5">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="bookQuantity">Book Quantity</label>
                                        <div class="col-sm-9">
                                            <input type="number" id="bookQuantity" name="bookQuantity" pattern="[0-9]{1,4}" required placeholder="Book Quantity" class="col-xs-10 col-sm-5">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="bookCategory">Book Category</label>
                                        <div class="col-sm-9">
                                            <select id="bookCategory" name="bookCategory" required class="multiselect" multiple="">
                                                <s:if test="bookCategoryInfos.size()!=0">
                                                    <s:iterator value="bookCategoryInfos">
                                                        <option value="<s:property value="bookCategoryId"/>"><s:property value="bookCategoryName"/></option>
                                                    </s:iterator>
                                                </s:if>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="bookLocation">Book Location</label>
                                        <div class="col-sm-9">
                                            <textarea id="bookLocation" name="bookLocation" placeholder="Book Location" required maxlength="100" class="col-xs-10 col-sm-5"></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="bookDescription">Book Description</label>
                                        <div class="col-sm-9">
                                            <textarea id="bookDescription" name="bookDescription" placeholder="Book Description" required maxlength="150" class="col-xs-10 col-sm-5"></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="bookDescription">Book Picture</label>
                                        <div class="col-sm-4">
                                            <input name="userImage" type="file" id="id-input-file-3" class="col-xs-10 col-sm-5">
                                            <label>
                                                <input type="checkbox" name="file-format" id="id-file-format" class="ace">
                                                <span class="lbl"> Allow only images</span>
                                            </label>
                                        </div>
                                    </div>

                                    <div class="clearfix form-actions">
                                        <div class="col-md-offset-3 col-md-9">
                                            <button class="btn btn-info" type="submit">
                                                <i class="ace-icon fa fa-check bigger-110"></i>
                                                Submit
                                            </button>
                                            &nbsp; &nbsp; &nbsp;
                                            <button class="btn" type="reset">
                                                <i class="ace-icon fa fa-undo bigger-110"></i>
                                                Reset
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.page-content -->
                </div>
            </div>
            <!-- /.main-content -->

            <%@include file="/start/footer.jsp" %>
        </div>
        <!-- /.main-container -->

        <!-- basic scripts -->
        <%@include file="/start/footer_resources.jsp" %>

        <script src="<%= request.getContextPath()%>/js/chosen.jquery.min.js"></script>
        <script src="<%= request.getContextPath()%>/js/spinbox.min.js"></script>
        <script src="<%= request.getContextPath()%>/js/bootstrap-datepicker.min.js"></script>
        <script src="<%= request.getContextPath()%>/js/bootstrap-timepicker.min.js"></script>
        <script src="<%= request.getContextPath()%>/js/moment.min.js"></script>
        <script src="<%= request.getContextPath()%>/js/daterangepicker.min.js"></script>
        <script src="<%= request.getContextPath()%>/js/bootstrap-datetimepicker.min.js"></script>
        <script src="<%= request.getContextPath()%>/js/bootstrap-colorpicker.min.js"></script>
        <script src="<%= request.getContextPath()%>/js/jquery.knob.min.js"></script>
        <script src="<%= request.getContextPath()%>/js/autosize.min.js"></script>
        <script src="<%= request.getContextPath()%>/js/jquery.inputlimiter.min.js"></script>
        <script src="<%= request.getContextPath()%>/js/jquery.maskedinput.min.js"></script>
        <script src="<%= request.getContextPath()%>/js/bootstrap-tag.min.js"></script>

        <script src="<%= request.getContextPath()%>/js/bootstrap-multiselect.min.js"></script>

        <!-- inline scripts related to this page -->
        <script type="text/javascript">
            jQuery(function ($) {

                $('#id-input-file-3').ace_file_input({
                    style: 'well',
                    btn_choose: 'Drop files here or click to choose',
                    btn_change: null,
                    no_icon: 'ace-icon fa fa-cloud-upload',
                    droppable: true,
                    thumbnail: 'small'//large | fit
                            //,icon_remove:null//set null, to hide remove/reset button
                            /**,before_change:function(files, dropped) {
                             //Check an example below
                             //or examples/file-upload.html
                             return true;
                             }*/
                            /**,before_remove : function() {
                             return true;
                             }*/
                    ,
                    preview_error: function (filename, error_code) {
                        //name of the file that failed
                        //error_code values
                        //1 = 'FILE_LOAD_FAILED',
                        //2 = 'IMAGE_LOAD_FAILED',
                        //3 = 'THUMBNAIL_FAILED'
                        //alert(error_code);
                    }

                }).on('change', function () {
                    //console.log($(this).data('ace_input_files'));
                    //console.log($(this).data('ace_input_method'));
                });

                //dynamically change allowed formats by changing allowExt && allowMime function
                $('#id-file-format').removeAttr('checked').on('change', function () {
                    var whitelist_ext, whitelist_mime;
                    var btn_choose;
                    var no_icon;
                    if (this.checked) {
                        btn_choose = "Drop images here or click to choose";
                        no_icon = "ace-icon fa fa-picture-o";

                        whitelist_ext = ["jpeg", "jpg", "png", "gif", "bmp"];
                        whitelist_mime = ["image/jpg", "image/jpeg", "image/png", "image/gif", "image/bmp"];
                    } else {
                        btn_choose = "Drop files here or click to choose";
                        no_icon = "ace-icon fa fa-cloud-upload";

                        whitelist_ext = null;//all extensions are acceptable
                        whitelist_mime = null;//all mimes are acceptable
                    }
                    var file_input = $('#id-input-file-3');
                    file_input
                            .ace_file_input('update_settings',
                                    {
                                        'btn_choose': btn_choose,
                                        'no_icon': no_icon,
                                        'allowExt': whitelist_ext,
                                        'allowMime': whitelist_mime
                                    })
                    file_input.ace_file_input('reset_input');

                    file_input
                            .off('file.error.ace')
                            .on('file.error.ace', function (e, info) {
                                //console.log(info.file_count);//number of selected files
                                //console.log(info.invalid_count);//number of invalid files
                                //console.log(info.error_list);//a list of errors in the following format
                            });
                });

                $('.multiselect').multiselect({
                    enableFiltering: true,
                    enableHTML: true,
                    buttonClass: 'btn btn-white btn-primary',
                    templates: {
                        button: '<button type="button" class="multiselect dropdown-toggle" data-toggle="dropdown"><span class="multiselect-selected-text"></span> &nbsp;<b class="fa fa-caret-down"></b></button>',
                        ul: '<ul class="multiselect-container dropdown-menu"></ul>',
                        filter: '<li class="multiselect-item filter"><div class="input-group"><span class="input-group-addon"><i class="fa fa-search"></i></span><input class="form-control multiselect-search" type="text"></div></li>',
                        filterClearBtn: '<span class="input-group-btn"><button class="btn btn-default btn-white btn-grey multiselect-clear-filter" type="button"><i class="fa fa-times-circle red2"></i></button></span>',
                        li: '<li><a tabindex="0"><label></label></a></li>',
                        divider: '<li class="multiselect-item divider"></li>',
                        liGroup: '<li class="multiselect-item multiselect-group"><label></label></li>'
                    }
                });
            });
        </script>
    </body>
</html>
