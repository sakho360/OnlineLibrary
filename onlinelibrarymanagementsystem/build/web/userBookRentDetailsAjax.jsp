<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<s:if test="userInfoList.size() !=0">
    <s:iterator value="userInfoList">
        <form action="UpdateUserBookRent" method="post" class="form-horizontal" role="form">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header no-padding">
                        <div class="table-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                <span class="white">&times;</span>
                            </button>
                            Results for User Book Rent Details
                        </div>
                    </div>

                    <div class="modal-body no-padding">
                        <table class="table table-striped table-bordered table-hover no-margin-bottom no-border-top">
                            <thead>
                                <tr>
                                    <th>User Id</th>
                                    <th><span class="col-xs-10 col-sm-10"><s:property value="userid"/></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>User Name</td>
                                    <td><span class="col-xs-10 col-sm-10"><s:property value="fullName"/></span></td>
                                </tr>
                                <tr>
                                    <td>Book Id</td>
                                    <td>
                                        <span class="col-xs-10 col-sm-10">
                                            <s:iterator value="bookInfos">
                                                <s:property value="bookId"/>
                                                <label>
                                                    <input id="rbBookRet" name="rbBookRet" type="radio" class="ace">
                                                    <span class="lbl"> Book Return</span>
                                                </label>
                                                <br>
                                            </s:iterator>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Book Name</td>
                                    <td>
                                        <span class="col-xs-10 col-sm-10">
                                            <s:iterator value="bookInfos">
                                                <s:property value="bookName"/><br>
                                            </s:iterator>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Book Reference</td>
                                    <td>
                                        <span class="col-xs-10 col-sm-10">
                                            <s:iterator value="bookInfos">
                                                <s:property value="bookRef"/><br>
                                            </s:iterator>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Book Title</td>
                                    <td>
                                        <span class="col-xs-10 col-sm-10">
                                            <s:iterator value="bookInfos">
                                                <s:property value="bookTitle"/><br>
                                            </s:iterator>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Book Rent</td>
                                    <td>
                                        <span class="col-xs-10 col-sm-10">
                                            <s:iterator value="bookInfos">
                                                <s:property value="bookRent"/><br>
                                            </s:iterator>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Rent Date</td>
                                    <td>
                                        <span class="col-xs-10 col-sm-10">
                                            <s:iterator value="bookInfos">
                                                <s:property value="rentDate"/><br>
                                            </s:iterator>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Return Date</td>
                                    <td>
                                        <span class="col-xs-10 col-sm-10">
                                            <s:iterator value="bookInfos">
                                                <s:property value="returnDate"/><br>
                                            </s:iterator>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Book Image</td>
                                    <td>
                                        <span class="col-xs-10 col-sm-10">
                                            <s:iterator value="bookInfos">
                                                <img alt="Book Image Not Found" src="UPLOADED_IMAGE/<s:property value="imgUrl"/>" width="200" height="100"/><br>
                                            </s:iterator>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: center;" colspan="2">

                                        <input type="hidden" id="userId" name="userId" value="<s:property value="userid"/>"/>

                                        <button type="submit" class="btn btn-info">
                                            <i class="ace-icon fa fa-check bigger-110"></i>
                                            Update
                                        </button>

                                        <button type="reset" class="btn">
                                            <i class="ace-icon fa fa-undo bigger-110"></i>
                                            Reset
                                        </button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="modal-footer no-margin-top">
                        <button class="btn btn-sm btn-danger pull-left" data-dismiss="modal">
                            <i class="ace-icon fa fa-times"></i>
                            Close
                        </button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
        </form>
        <!-- /.modal-dialog -->
    </s:iterator>
</s:if>
