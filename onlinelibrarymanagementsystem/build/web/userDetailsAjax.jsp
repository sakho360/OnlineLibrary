<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<s:if test="userInfoList != null ">
    <s:iterator value="userInfoList">
        <form action="UpdateUser" method="post" class="form-horizontal" role="form" enctype="multipart/form-data">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header no-padding">
                        <div class="table-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                <span class="white">&times;</span>
                            </button>
                            Results for Details of <s:property value="fullName"/>
                        </div>
                    </div>

                    <div class="modal-body no-padding">
                        <table class="table table-striped table-bordered table-hover no-margin-bottom no-border-top">
                            <thead>
                                <tr>
                                    <th><s:property value="userGroupInfo.groupName"/> Id</th>
                                    <th><span class="col-xs-10 col-sm-10"><s:property value="userid"/></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>First Name</td>
                                    <td>
                                        <div class="col-sm-9">
                                            <input value="<s:property value="firstName"/>" type="text" id="firstName" name="firstName" required pattern="[a-zA-Z0-9-. ]{3,20}" placeholder="First Name" class="col-xs-10 col-sm-10">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Last Name</td>
                                    <td>
                                        <div class="col-sm-9">
                                            <input value="<s:property value="lastName"/>" type="text" id="lastName" name="lastName" pattern="[a-zA-Z0-9-. ]{3,20}" required placeholder="Last Name" class="col-xs-10 col-sm-10">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>User Name</td>
                                    <td>
                                        <div class="col-sm-9">
                                            <input value="<s:property value="userName"/>" type="text" id="userName" name="userName" pattern="[a-z0-9]{3,20}" required placeholder="User Name" class="col-xs-10 col-sm-10">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Date of Birth</td>
                                    <td>
                                        <div class="col-sm-7">
                                            <div class="input-group">
                                                <input value="<s:property value="userDob"/>" class="form-control date-picker" name="dob" placeholder="yyyy-mm-dd" required id="id-date-picker-1" type="text" data-date-format="yyyy-mm-dd" />
                                                <span class="input-group-addon">
                                                    <i class="fa fa-calendar bigger-110"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Mobile</td>
                                    <td>
                                        <div class="col-sm-9">
                                            <input value="<s:property value="userMobile"/>" type="text" id="mobile" name="mobile" pattern="[0-9]{8,15}" required placeholder="Mobile" class="col-xs-10 col-sm-10">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Gender</td>
                                    <td>
                                        <span class="col-xs-10 col-sm-10">
                                            <s:if test="gender==1">
                                                <div class="form-group">
                                                    <div class="radio">
                                                        <label>
                                                            <input id="gender" name="gender" type="radio" value="1" class="ace" checked/>
                                                            <span class="lbl"> Male</span>
                                                        </label>
                                                        <label>
                                                            <input id="gender" name="gender" type="radio" value="2" class="ace"/>
                                                            <span class="lbl"> Female</span>
                                                        </label>
                                                        <label>
                                                            <input id="gender" name="gender" type="radio" value="3" class="ace"/>
                                                            <span class="lbl"> Others</span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </s:if>
                                            <s:elseif test="gender==2">
                                                <div class="form-group">
                                                    <div class="radio">
                                                        <label>
                                                            <input id="gender" name="gender" type="radio" value="1" class="ace"/>
                                                            <span class="lbl"> Male</span>
                                                        </label>
                                                        <label>
                                                            <input id="gender" name="gender" type="radio" value="2" class="ace" checked/>
                                                            <span class="lbl"> Female</span>
                                                        </label>
                                                        <label>
                                                            <input id="gender" name="gender" type="radio" value="3" class="ace"/>
                                                            <span class="lbl"> Others</span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </s:elseif>
                                            <s:else>
                                                <div class="form-group">
                                                    <div class="radio">
                                                        <label>
                                                            <input id="gender" name="gender" type="radio" value="1" class="ace"/>
                                                            <span class="lbl"> Male</span>
                                                        </label>
                                                        <label>
                                                            <input id="gender" name="gender" type="radio" value="2" class="ace"/>
                                                            <span class="lbl"> Female</span>
                                                        </label>
                                                        <label>
                                                            <input id="gender" name="gender" type="radio" value="3" class="ace" checked/>
                                                            <span class="lbl"> Others</span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </s:else>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Email</td>
                                    <td>
                                        <div class="col-sm-9">
                                            <input value="<s:property value="userEmail"/>" type="email" id="email" name="email" pattern="[a-zA-Z0-9-.-_]{3,30}" required placeholder="Email" class="col-xs-10 col-sm-10">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Occupation</td>
                                    <td>
                                        <div class="col-sm-9">
                                            <input value="<s:property value="occupation"/>" type="text" id="occupation" name="occupation" pattern="[a-zA-Z0-9-. ]{3,20}" required placeholder="Occupation" class="col-xs-10 col-sm-10">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Address</td>
                                    <td>
                                        <div class="col-sm-9">
                                            <textarea id="address" name="address" placeholder="Address" required maxlength="150" class="col-xs-10 col-sm-10"><s:property value="userAddress"/></textarea>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Status</td>
                                    <td>
                                        <span class="col-xs-10 col-sm-10">
                                            <select id="userStatus" name="userStatus">
                                                <s:if test="userStatus=='Y'">
                                                    <option selected value="Y">Active</option>
                                                    <option value="N">Inactive</option>
                                                </s:if>
                                                <s:else>
                                                    <option value="Y">Active</option>
                                                    <option selected value="N">Inactive</option>
                                                </s:else>
                                            </select>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td><s:property value="userGroupInfo.groupName"/> Image</td>
                                    <td>
                                        <span class="col-xs-10 col-sm-10">
                                            <img  alt="<s:property value="userGroupInfo.groupName"/> Image Not Found" src="UPLOADED_IMAGE/<s:property value="userImage"/>" width="200" height="100"/>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: center;" colspan="2">

                                        <input type="hidden" id="userid" name="userid" value="<s:property value="userid"/>">
                                        <input type="hidden" id="userType" name="userType" value="<s:property value="userGroupInfo.groupName"/>">

                                        <button type="submit" class="btn btn-info">
                                            <i class="ace-icon fa fa-check bigger-110"></i>
                                            Update
                                        </button>

                                        <button type="reset" class="btn">
                                            <i class="ace-icon fa fa-undo bigger-110"></i>
                                            Reset
                                        </button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="modal-footer no-margin-top">
                        <button class="btn btn-sm btn-danger pull-left" data-dismiss="modal">
                            <i class="ace-icon fa fa-times"></i>
                            Close
                        </button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
        </form>
        <!-- /.modal-dialog -->
    </s:iterator>
</s:if>
<script type="text/javascript">
    jQuery(function ($) {
        $('.date-picker').datepicker({
            autoclose: true,
            todayHighlight: true
        })
                //show datepicker when clicking on the icon
                .next().on(ace.click_event, function () {
            $(this).prev().focus();
        });
    });
</script>
