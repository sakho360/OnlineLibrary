<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="/start/header_resources.jsp" %>

        <script type="text/javascript">
            function parentMenuCheckBox(chkBxId, id) {

                var chckbox = document.getElementById(chkBxId);

                if (chckbox.checked) {

                    document.getElementsByName("parentMenu").checked = true;

                    $('.chkbox' + id).each(function () {

                        this.checked = true;
                    });
                } else {
                    $('.chkbox' + id).each(function () {

                        this.checked = false;
                    });

                    if ($("input:checkbox:checked").length == 1) {

                        document.getElementsByName("parentMenu").checked = false;
                    }
                }
            }

            function subMenuCheckBox(chkBxId, pchkBxId, id) {

                var chckbox = document.getElementById(chkBxId);

                if (chckbox.checked) {

                    document.getElementsByName("parentMenu").checked = true;

                    document.getElementById(pchkBxId).checked = true;

                } else {

                    if ($(".chkbox" + id + ":checked").length == 0) {

                        document.getElementById(pchkBxId).checked = false;
                    }

                    if ($("input:checkbox:checked").length == 1) {

                        document.getElementsByName("parentMenu").checked = false;
                    }
                }
            }

            function menuShowHide(collapseSubMenu) {

                for (var i = 1; i < collapseSubMenu.length; i++) {
                    $("." + collapseSubMenu + i).hide();
                }
                $("." + collapseSubMenu).slideToggle("slow");
            }
        </script>

        <style type="text/css">

            table {
                width: 100%
            }

            .chk-child {
                padding-left: 20px;
                text-align: center;
            }

            .cld-name {
                text-align: left; 
                padding-left: 3px;
            }

            .tbl-tr {
                background-color: #336ea5; 
                font-size: 12px;
                color: #ffffff;
            }

            .td-icon {
                cursor:pointer; 
                text-align: center;
            }

            .mas-chk {
                text-align: center;
            }
        </style>

    </head>
    <body class="no-skin">

        <%@include file="/start/top_bar.jsp" %>

        <div class="main-container ace-save-state" id="main-container">

            <%@include file="/start/left_manu.jsp" %>

            <div class="main-content">
                <div class="main-content-inner">
                    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
                        <ul class="breadcrumb">
                            <li>
                                <i class="ace-icon fa fa-home home-icon"></i>
                                <a href="Dashboard">Home</a>
                            </li>

                            <li>
                                <a href="CreateAdminUser">Admin User</a>
                            </li>
                            <li class="active">Create Admin User</li>
                        </ul>
                        <!-- /.breadcrumb -->

                        <div class="nav-search" id="nav-search">
                            <form class="form-search">
                                <span class="input-icon">
                                    <input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
                                    <i class="ace-icon fa fa-search nav-search-icon"></i>
                                </span>
                            </form>
                        </div>
                        <!-- /.nav-search -->
                    </div>

                    <div class="page-content">

                        <%@include file="/start/settings_box.jsp" %>

                        <div class="page-header">
                            <h1>
                                Admin User
                                <small>
                                    <i class="ace-icon fa fa-angle-double-right"></i>
                                    Set Details of Admin User
                                </small>
                            </h1>
                        </div>
                        <!-- /.page-header -->

                        <div class="row">
                            <div class="col-xs-12">
                                <!-- PAGE CONTENT BEGINS -->

                                <h4 class="blue smaller">
                                    <i class="fa fa-tags"></i>
                                    Category
                                </h4>

                                <div class="tree-container">
                                    <ul id="cat-tree"></ul>
                                </div>

                                <form action="SaveMenuPermission" method="post" class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Select User Group</label>
                                        <div class="col-sm-4">
                                            <select id="userGroupId" name="userGroupId" required class="form-control">
                                                <option selected value="">Select User Group</option>
                                                <s:if test="userGroupInfoList.size() != 0 ">
                                                    <s:iterator value="userGroupInfoList">
                                                        <option value="<s:property value="userGroupID"/>"><s:property value="groupName"/></option>
                                                    </s:iterator>
                                                </s:if>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Menu</label>
                                        <div class="col-sm-4">
                                            <table id="groupFormTbl">
                                                <tbody id="tbl-menu">
                                                    <% int mpid = 0;%>
                                                    <s:if test="menuMasterInfoList.size() != 0 ">
                                                        <s:iterator value="menuMasterInfoList">
                                                            <% mpid++;%>
                                                            <% int mcid = 0;%>
                                                            <tr class="tbl-tr">
                                                                <td class="mas-chk">
                                                                    <s:if test="masterMenuStatus == 'Y' ">
                                                                        <input type="checkbox" value="<s:property value="masterMenuID"/>" onclick="parentMenuCheckBox('PR<%=mpid%>CH<%=mcid%>', '<%=mpid%>');" id="PR<%=mpid%>CH<%=mcid%>" name="masterMenuID" checked>
                                                                    </s:if>
                                                                    <s:else>
                                                                        <input type="checkbox" value="<s:property value="masterMenuID"/>" onclick="parentMenuCheckBox('PR<%=mpid%>CH<%=mcid%>', '<%=mpid%>');" id="PR<%=mpid%>CH<%=mcid%>" name="masterMenuID">
                                                                    </s:else>
                                                                </td>

                                                                <td class="cld-name">
                                                                    <s:property value="masterMenuName"/>
                                                                </td>

                                                                <s:if test="menuChildList != null">
                                                                    <s:if test="menuChildList.size()!=0">
                                                                        <td onclick="menuShowHide('collapseMenu<%=mpid%>')" class="td-icon">[-]</td>
                                                                    </s:if>
                                                                    <s:else>
                                                                        <td></td>
                                                                    </s:else>
                                                                </s:if>
                                                                <s:else>
                                                                    <td></td>
                                                                </s:else>
                                                            </tr>

                                                            <s:iterator value="menuChildList">
                                                                <% mcid++;%>
                                                                <tr class="chm tbl-tr collapseMenu<%=mpid%>">
                                                                    <td class="chk-child">
                                                                        <s:if test="childMenuStatus == 'Y' ">
                                                                            <input type="checkbox" value="<s:property value="childMenuID"/>" onclick="subMenuCheckBox('PR<%=mpid%>CH<%=mcid%>', 'PR<%=mpid%>CH0', '<%=mpid%>');" class="chkbox<%=mpid%>" id="PR<%=mpid%>CH<%=mcid%>" name="childMenuID" checked>
                                                                        </s:if>
                                                                        <s:else>
                                                                            <input type="checkbox" value="<s:property value="childMenuID"/>" onclick="subMenuCheckBox('PR<%=mpid%>CH<%=mcid%>', 'PR<%=mpid%>CH0', '<%=mpid%>');" class="chkbox<%=mpid%>" id="PR<%=mpid%>CH<%=mcid%>" name="childMenuID">
                                                                        </s:else>
                                                                    </td>
                                                                    <td class="cld-name">
                                                                        <s:property value="childMenuName"/>
                                                                    </td>
                                                                </tr>
                                                            </s:iterator>
                                                        </s:iterator>
                                                    </s:if>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                    <div class="clearfix form-actions">
                                        <div class="col-md-offset-3 col-md-9">
                                            <input type="hidden" id="formMenuIdStatus" name="formMenuIdStatus"/>
                                            <button class="btn btn-info" type="submit">
                                                <i class="ace-icon fa fa-check bigger-110"></i>
                                                Submit
                                            </button>
                                            &nbsp; &nbsp; &nbsp;
                                            <button class="btn" type="reset">
                                                <i class="ace-icon fa fa-undo bigger-110"></i>
                                                Reset
                                            </button>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.page-content -->
                </div>
            </div>
            <!-- /.main-content -->

            <%@include file="/start/footer.jsp" %>
        </div>
        <!-- /.main-container -->

        <!-- basic scripts -->
        <%@include file="/start/footer_resources.jsp" %>

        <!--<script src="<%= request.getContextPath()%>/js/tree.min.js"></script>-->

        <!-- inline scripts related to this page -->

        <script type="text/javascript">
            $(document).ready(function () {
                $('.chm').hide();

                $('form').submit(function () {
                    var formMenuIdStatus = '';
                    $(this).find('input[type="checkbox"]').each(function () {
                        var checkbox = $(this);
                        if (checkbox.is(':checked')) {
                            formMenuIdStatus += checkbox.val() + '/mid/Y/mstatus/';
                        } else {
                            formMenuIdStatus += checkbox.val() + '/mid/N/mstatus/';
                        }
                    });

                    $('#formMenuIdStatus').val(formMenuIdStatus);
                });

                $("#userGroupId").change(function () {
                    var ugId = $(this).find(":selected").val();
                    $.post("MenuBySelectedGroup", {userGroupId: ugId}, function (result) {
                        $("#tbl-menu").html(result);
                    });
                });
            });
        </script>

        <script type="text/javascript">
            jQuery(function ($) {
                //data for tree element
                var category = {
                    'for-sale': {text: 'For Sale', type: 'folder'},
                    'vehicles': {text: 'Vehicles', type: 'folder'},
                    'rentals': {text: 'Rentals', type: 'item'},
                    'real-estate': {text: 'Real Estate', type: 'item'},
                    'pets': {text: 'Pets', type: 'item'},
                    'tickets': {text: 'Tickets', type: 'item'}
                }
                category['for-sale']['additionalParameters'] = {
                    'children': {
                        'appliances': {text: 'Appliances', type: 'item'},
                        'arts-crafts': {text: 'Arts & Crafts', type: 'item'},
                        'clothing': {text: 'Clothing', type: 'item'},
                        'computers': {text: 'Computers', type: 'item'},
                        'jewelry': {text: 'Jewelry', type: 'item'},
                        'office-business': {text: 'Office', type: 'item'},
                        'sports-fitness': {text: 'Sports & Fitness', type: 'item'}
                    }
                }
                category['vehicles']['additionalParameters'] = {
                    'children': {
                        'appliances': {text: 'Appliances', type: 'item'},
                        'arts-crafts': {text: 'Arts & Crafts', type: 'item'},
                        'clothing': {text: 'Clothing', type: 'item'},
                        'computers': {text: 'Computers', type: 'item'},
                        'jewelry': {text: 'Jewelry', type: 'item'},
                        'office-business': {text: 'Office', type: 'item'},
                        'sports-fitness': {text: 'Sports & Fitness', type: 'item'}
                    }
                }

                var dataSource1 = function (options, callback) {
                    var $data = null;
                    if (!("text" in options) && !("type" in options)) {
                        $data = category;//the root tree
                        callback({data: $data});
                        return;
                    } else if ("type" in options && options.type === "folder") {
                        if ("additionalParameters" in options && "children" in options.additionalParameters)
                            $data = options.additionalParameters.children || {};
                        else
                            $data = {}//no data
                    }

                    callback({data: $data})
                }

                $('#cat-tree').ace_tree({
                    dataSource: dataSource1,
                    multiSelect: true,
                    cacheItems: true,
                    'open-icon': 'ace-icon tree-minus',
                    'close-icon': 'ace-icon tree-plus',
                    'itemSelect': true,
                    'folderSelect': false,
                    'selected-icon': 'ace-icon fa fa-check',
                    'unselected-icon': 'ace-icon fa fa-times',
                    loadingHTML: '<div class="tree-loading"><i class="ace-icon fa fa-refresh fa-spin blue"></i></div>'
                });


                $('.tree-container').ace_scroll({size: 250, mouseWheelLock: true});
                $('#cat-tree').on('closed.fu.tree disclosedFolder.fu.tree', function () {
                    $('.tree-container').ace_scroll('reset').ace_scroll('start');
                });
            });
        </script>
    </body>
</html>
