<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="s" uri="/struts-tags"%>
<div class="alert alert-<s:property value="messageColor"/> fade in">
    <button class="close" data-dismiss="alert">
        <i class="ace-icon fa fa-times"></i>
    </button>
    <s:property value="messageString"/>
</div>
