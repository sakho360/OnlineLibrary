<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<s:if test="bookInfoList != null ">
    <s:iterator value="bookInfoList">
        <form action="UpdateBook" method="post" class="form-horizontal" role="form" enctype="multipart/form-data">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header no-padding">
                        <div class="table-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                <span class="white">&times;</span>
                            </button>
                            Results for Book Details
                        </div>
                    </div>

                    <div class="modal-body no-padding">
                        <table class="table table-striped table-bordered table-hover no-margin-bottom no-border-top">
                            <thead>
                                <tr>
                                    <th>Book Id</th>
                                    <th><span class="col-xs-10 col-sm-10"><s:property value="bookId"/></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Book Name</td>
                                    <td>
                                        <div class="col-sm-9">
                                            <input value="<s:property value="bookName"/>" type="text" id="bookName" name="bookName" pattern="[a-zA-Z0-9-. ]{3,20}" required placeholder="Book Name" class="col-xs-10 col-sm-10">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Book Reference</td>
                                    <td>
                                        <div class="col-sm-9">
                                            <input value="<s:property value="bookRef"/>" type="text" id="bookReference" name="bookReference" pattern="[a-zA-Z0-9-. ]{3,20}" required placeholder="Book Reference Code" class="col-xs-10 col-sm-10">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Book Title</td>
                                    <td>
                                        <div class="col-sm-9">
                                            <input value="<s:property value="bookTitle"/>" type="text" id="bookTitle" name="bookTitle" pattern="[a-zA-Z0-9-. ]{3,20}" required placeholder="Book Title" class="col-xs-10 col-sm-10">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Book Quantity</td>
                                    <td>
                                        <div class="col-sm-9">
                                            <input value="<s:property value="bookQuantity"/>" type="number" id="bookQuantity" name="bookQuantity" pattern="[0-9]{1,4}" required placeholder="Book Quantity" class="col-xs-10 col-sm-10">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Book Rent</td>
                                    <td><span class="col-xs-10 col-sm-10"><s:property value="bookRent"/></span></td>
                                </tr>
                                <tr>
                                    <td>Book Stock</td>
                                    <td><span class="col-xs-10 col-sm-10"><s:property value="bookCurStock"/></span></td>
                                </tr>
                                <tr>
                                    <td>Book Category</td>
                                    <td>
                                        <div class="col-sm-9">
                                            <select id="bookCategory" name="bookCategory" required class="multiselect" multiple="">
                                                <s:if test="bookCategoryInfos.size()!=0">
                                                    <s:iterator value="bookCategoryInfos">
                                                        <option value="<s:property value="bookCategoryId"/>"><s:property value="bookCategoryName"/></option>
                                                    </s:iterator>
                                                </s:if>
                                            </select>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Book Location</td>
                                    <td>
                                        <div class="col-sm-9">
                                            <textarea id="bookLocation" name="bookLocation" placeholder="Book Location" required maxlength="100" class="col-xs-10 col-sm-10"><s:property value="bookLocation"/></textarea>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Book Description</td>
                                    <td>
                                        <div class="col-sm-9">
                                            <textarea id="bookDescription" name="bookDescription" placeholder="Book Description" required maxlength="150" class="col-xs-10 col-sm-10"><s:property value="description"/></textarea>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Upload Date</td>
                                    <td><span class="col-xs-10 col-sm-10"><s:property value="insertDate"/></span></td>
                                </tr>
                                <tr>
                                    <td>Status</td>
                                    <td>
                                        <span class="col-xs-10 col-sm-10">
                                            <select id="status" name="status">
                                                <s:if test="status=='Y'">
                                                    <option selected value="Y">Active</option>
                                                    <option value="N">Inactive</option>
                                                </s:if>
                                                <s:else>
                                                    <option value="Y">Active</option>
                                                    <option selected value="N">Inactive</option>
                                                </s:else>
                                            </select>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Book Image</td>
                                    <td><span class="col-xs-10 col-sm-10"><img alt="Book Image Not Found" src="UPLOADED_IMAGE/<s:property value="imgUrl"/>" width="200" height="100"/></span></td>
                                </tr>
                                <tr>
                                    <td style="text-align: center;" colspan="2">

                                        <input type="hidden" id="bookId" name="bookId" value="<s:property value="bookId"/>"/>

                                        <button type="submit" class="btn btn-info">
                                            <i class="ace-icon fa fa-check bigger-110"></i>
                                            Update
                                        </button>

                                        <button type="reset" class="btn">
                                            <i class="ace-icon fa fa-undo bigger-110"></i>
                                            Reset
                                        </button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="modal-footer no-margin-top">
                        <button class="btn btn-sm btn-danger pull-left" data-dismiss="modal">
                            <i class="ace-icon fa fa-times"></i>
                            Close
                        </button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
        </form>
        <!-- /.modal-dialog -->
    </s:iterator>
</s:if>
<script type="text/javascript">
    jQuery(function ($) {
        $('.multiselect').multiselect({
            enableFiltering: true,
            enableHTML: true,
            buttonClass: 'btn btn-white btn-primary',
            templates: {
                button: '<button type="button" class="multiselect dropdown-toggle" data-toggle="dropdown"><span class="multiselect-selected-text"></span> &nbsp;<b class="fa fa-caret-down"></b></button>',
                ul: '<ul class="multiselect-container dropdown-menu"></ul>',
                filter: '<li class="multiselect-item filter"><div class="input-group"><span class="input-group-addon"><i class="fa fa-search"></i></span><input class="form-control multiselect-search" type="text"></div></li>',
                filterClearBtn: '<span class="input-group-btn"><button class="btn btn-default btn-white btn-grey multiselect-clear-filter" type="button"><i class="fa fa-times-circle red2"></i></button></span>',
                li: '<li><a tabindex="0"><label></label></a></li>',
                divider: '<li class="multiselect-item divider"></li>',
                liGroup: '<li class="multiselect-item multiselect-group"><label></label></li>'
            }
        });
    });
</script>
