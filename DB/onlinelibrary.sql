/*
SQLyog Ultimate v8.55 
MySQL - 5.0.22-community-nt : Database - onlinelibrary
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`onlinelibrary` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `onlinelibrary`;

/*Table structure for table `book_category` */

DROP TABLE IF EXISTS `book_category`;

CREATE TABLE `book_category` (
  `BOOK_CATEGORY_ID` int(11) NOT NULL auto_increment,
  `BOOK_CATEGORY_NAME` varchar(50) default NULL,
  `DESCRIPTION` text,
  `STATUS` char(1) default 'Y',
  `INSERT_BY` varchar(50) default NULL,
  `INSERT_DATE` timestamp NULL default NULL,
  `UPDATE_BY` varchar(50) default NULL,
  `UPDATE_DATE` timestamp NULL default NULL,
  `DELETE_BY` varchar(50) default NULL,
  `DELETE_DATE` timestamp NULL default NULL,
  PRIMARY KEY  (`BOOK_CATEGORY_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `book_category` */

insert  into `book_category`(`BOOK_CATEGORY_ID`,`BOOK_CATEGORY_NAME`,`DESCRIPTION`,`STATUS`,`INSERT_BY`,`INSERT_DATE`,`UPDATE_BY`,`UPDATE_DATE`,`DELETE_BY`,`DELETE_DATE`) values (1,'Science','Science Book','Y','100001','2017-06-20 16:29:29',NULL,NULL,NULL,NULL),(2,'Humanities','Humanities Book','Y','100001','2017-06-20 16:30:24',NULL,NULL,NULL,NULL),(3,'7uytu','ytuytuyt','Y','100001','2017-07-04 20:08:23',NULL,NULL,NULL,NULL),(4,'Novel','Novel Novel','Y','100004','2017-07-08 20:44:28',NULL,NULL,NULL,NULL);

/*Table structure for table `book_info` */

DROP TABLE IF EXISTS `book_info`;

CREATE TABLE `book_info` (
  `BOOK_ID` varchar(15) NOT NULL,
  `BOOK_REF` varchar(50) default NULL,
  `BOOK_NAME` varchar(50) default NULL,
  `BOOK_TITLE` varchar(50) default NULL,
  `BOOK_QUANTITY` int(11) default '0',
  `BOOK_LOCATION` text,
  `BOOK_DESCRIPTION` text,
  `BOOK_CATEGORY` varchar(20) default NULL,
  `IMG_URL` varchar(15) default NULL,
  `BOOK_STATUS` char(1) default 'Y',
  `INSERT_BY` varchar(50) default NULL,
  `INSERT_DATE` timestamp NULL default NULL,
  `UPDATE_BY` varchar(50) default NULL,
  `UPDATE_DATE` timestamp NULL default NULL,
  `DELETE_BY` varchar(50) default NULL,
  `DELETE_DATE` timestamp NULL default NULL,
  PRIMARY KEY  (`BOOK_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `book_info` */

insert  into `book_info`(`BOOK_ID`,`BOOK_REF`,`BOOK_NAME`,`BOOK_TITLE`,`BOOK_QUANTITY`,`BOOK_LOCATION`,`BOOK_DESCRIPTION`,`BOOK_CATEGORY`,`IMG_URL`,`BOOK_STATUS`,`INSERT_BY`,`INSERT_DATE`,`UPDATE_BY`,`UPDATE_DATE`,`DELETE_BY`,`DELETE_DATE`) values ('10001','drdfg','Shahnama','fdgfdgf',44,'sfdsf','dsfdsfd','1','10001.jpg','Y','100001','2017-07-03 20:36:17',NULL,NULL,NULL,NULL),('10002','7ytyuytuytu','Jhilmil','yuiyi',76,'gyfjhg','ghjhg','1','10002.png','Y','100001','2017-07-04 16:18:09',NULL,NULL,NULL,NULL),('10003','trytry','Pother Pachalee','trytrytr',40,'gyfhghj','gjgjg','1','10003.png','Y','100001','2017-07-04 20:07:50',NULL,NULL,NULL,NULL),('10004','ASDD3333','Debdas','Debdas Debdas',11,'3  rack','Debdas Debda sDebdas','4','10004.jpg','Y','100004','2017-07-08 20:45:34',NULL,NULL,NULL,NULL);

/*Table structure for table `menu` */

DROP TABLE IF EXISTS `menu`;

CREATE TABLE `menu` (
  `MENU_ID` int(11) NOT NULL auto_increment,
  `MENU_NAME` varchar(30) default NULL,
  `MENU_TITLE` varchar(30) default NULL,
  `PARENT_MENU_ID` int(11) default NULL,
  `MENU_STATUS` char(1) default 'N',
  `INSERT_BY` varchar(30) default NULL,
  `INSERT_DATE` timestamp NULL default NULL,
  `UPDATE_BY` varchar(30) default NULL,
  `UPDATE_DATE` timestamp NULL default NULL,
  PRIMARY KEY  (`MENU_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `menu` */

insert  into `menu`(`MENU_ID`,`MENU_NAME`,`MENU_TITLE`,`PARENT_MENU_ID`,`MENU_STATUS`,`INSERT_BY`,`INSERT_DATE`,`UPDATE_BY`,`UPDATE_DATE`) values (1,'Dashboard','Dashboard',0,'N',NULL,NULL,NULL,NULL),(2,'Admin','Admin',0,'N',NULL,NULL,NULL,NULL),(3,'Create Admin User','Create Admin User',2,'N',NULL,NULL,NULL,NULL),(4,'All Admin Users','All Admin Users',2,'N',NULL,NULL,NULL,NULL),(5,'All Book Categories','All Book Categories',2,'N',NULL,NULL,NULL,NULL),(6,'All Books','All Books',2,'N',NULL,NULL,NULL,NULL),(7,'All Librarians','All Librarians',2,'N',NULL,NULL,NULL,NULL),(8,'All Students','All Students',2,'N',NULL,NULL,NULL,NULL),(9,'Settings','Settings',2,'N',NULL,NULL,NULL,NULL),(10,'Book','Book',0,'N',NULL,NULL,NULL,NULL),(11,'Book Category','Book Category',10,'N',NULL,NULL,NULL,NULL),(12,'Book Category History','Book Category History',10,'N',NULL,NULL,NULL,NULL),(13,'Book Upload','Book Upload',10,'N',NULL,NULL,NULL,NULL),(14,'Book History','Book History',10,'N',NULL,NULL,NULL,NULL),(15,'Librarian','Librarian',0,'N',NULL,NULL,NULL,NULL),(16,'Create Librarian','Create Librarian',15,'N',NULL,NULL,NULL,NULL),(17,'Librarian History','Librarian History',15,'N',NULL,NULL,NULL,NULL),(18,'Student','Student',0,'N',NULL,NULL,NULL,NULL),(19,'Create Student','Create Student',18,'N',NULL,NULL,NULL,NULL),(20,'Student History','Student History',18,'N',NULL,NULL,NULL,NULL),(21,'Calendar','Calendar',0,'N',NULL,NULL,NULL,NULL),(22,'Gallery','Gallery',0,'N',NULL,NULL,NULL,NULL);

/*Table structure for table `user_book_map` */

DROP TABLE IF EXISTS `user_book_map`;

CREATE TABLE `user_book_map` (
  `USER_BOOK_ID` int(11) NOT NULL auto_increment,
  `USER_ID` varchar(20) default NULL,
  `BOOK_ID` varchar(20) default NULL,
  `BOOK_RENT_DATE` date default NULL,
  `BOOK_RERUTN_DATE` date default NULL,
  `BOOK_RENT_QUANTITY` int(11) default NULL,
  `STATUS` char(1) default 'Y',
  `INSERT_BY` varchar(50) default NULL,
  `INSERT_DATE` timestamp NULL default NULL,
  `UPDATE_BY` varchar(50) default NULL,
  `UPDATE_DATE` timestamp NULL default NULL,
  `DELETE_BY` varchar(50) default NULL,
  `DELETE_DATE` timestamp NULL default NULL,
  PRIMARY KEY  (`USER_BOOK_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `user_book_map` */

insert  into `user_book_map`(`USER_BOOK_ID`,`USER_ID`,`BOOK_ID`,`BOOK_RENT_DATE`,`BOOK_RERUTN_DATE`,`BOOK_RENT_QUANTITY`,`STATUS`,`INSERT_BY`,`INSERT_DATE`,`UPDATE_BY`,`UPDATE_DATE`,`DELETE_BY`,`DELETE_DATE`) values (1,'100001','10001','2017-07-01','2017-07-08',1,'Y','100001','2017-07-05 12:00:09',NULL,NULL,NULL,NULL),(2,'100001','10002','2017-07-09','2017-08-03',2,'Y','100001','2017-07-05 13:05:35',NULL,NULL,NULL,NULL),(3,'100002','10001','2017-07-26','2017-07-05',2,'Y','100001','2017-07-08 20:11:52',NULL,NULL,NULL,NULL),(4,'100009','10004','2017-07-17','2017-07-05',2,'Y','100004','2017-07-08 20:46:44',NULL,NULL,NULL,NULL);

/*Table structure for table `user_group` */

DROP TABLE IF EXISTS `user_group`;

CREATE TABLE `user_group` (
  `GROUP_ID` int(11) NOT NULL auto_increment,
  `GROUP_NAME` varchar(30) default NULL,
  `DESCRIPTION` text,
  `STATUS` char(1) default 'Y',
  `INSERT_BY` varchar(50) default NULL,
  `INSERT_DATE` timestamp NULL default NULL,
  `UPDATE_BY` varchar(50) default NULL,
  `UPDATE_DATE` timestamp NULL default NULL,
  `DELETE_BY` varchar(50) default NULL,
  `DELETE_DATE` timestamp NULL default NULL,
  PRIMARY KEY  (`GROUP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `user_group` */

insert  into `user_group`(`GROUP_ID`,`GROUP_NAME`,`DESCRIPTION`,`STATUS`,`INSERT_BY`,`INSERT_DATE`,`UPDATE_BY`,`UPDATE_DATE`,`DELETE_BY`,`DELETE_DATE`) values (0,'Admin','Admin','Y',NULL,NULL,NULL,NULL,NULL,NULL),(1,'Admin User','Admin User','Y',NULL,NULL,NULL,NULL,NULL,NULL),(2,'Librarian','Librarian','Y',NULL,NULL,NULL,NULL,NULL,NULL),(3,'Student','Student','Y',NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `user_info` */

DROP TABLE IF EXISTS `user_info`;

CREATE TABLE `user_info` (
  `USER_ID` varchar(15) NOT NULL,
  `FIRST_NAME` varchar(50) default NULL,
  `LAST_NAME` varchar(50) default NULL,
  `USER_NAME` varchar(50) default NULL,
  `DOB` date default NULL,
  `PASSWORD` varchar(50) default NULL,
  `MOBILE` varchar(20) default NULL,
  `GENDER` tinyint(1) default NULL,
  `EMAIL` varchar(30) default NULL,
  `ADDRESS` text,
  `OCCUPATION` varchar(50) default NULL,
  `DESCRIPTION` text,
  `USER_IMG` varchar(15) default NULL,
  `GROUP_ID` varchar(20) default NULL,
  `STATUS` char(1) default 'Y',
  `INSERT_BY` varchar(50) default NULL,
  `INSERT_DATE` timestamp NULL default NULL,
  `UPDATE_BY` varchar(50) default NULL,
  `UPDATR_DATE` timestamp NULL default NULL,
  `DELETE_BY` varchar(50) default NULL,
  `DELETE_DATE` timestamp NULL default NULL,
  PRIMARY KEY  (`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `user_info` */

insert  into `user_info`(`USER_ID`,`FIRST_NAME`,`LAST_NAME`,`USER_NAME`,`DOB`,`PASSWORD`,`MOBILE`,`GENDER`,`EMAIL`,`ADDRESS`,`OCCUPATION`,`DESCRIPTION`,`USER_IMG`,`GROUP_ID`,`STATUS`,`INSERT_BY`,`INSERT_DATE`,`UPDATE_BY`,`UPDATR_DATE`,`DELETE_BY`,`DELETE_DATE`) values ('100001','System','Admin','systemadmin','2017-06-26','123456','01834623824',1,'st@gmail.com','Dhaka','Student','Admin Group','100001.jpg','0','Y','100001','2017-06-19 20:20:20',NULL,NULL,NULL,NULL),('100002','MD.','Ashraful','ashraful','2017-05-28','ashraful','01915668877',1,'ashraful@ashraful','Athlete Athlete','Athlete','Athlete','100002.jpg','1','Y','100001','2017-07-04 19:51:31',NULL,NULL,NULL,NULL),('100003','Tamima','Iqbal','tamima','2017-07-16','tamim','01715884477',2,'tamima@tamima','tamima tamima','B B','tamima','100003.gif','3','Y','100001','2017-07-04 19:56:44',NULL,NULL,NULL,NULL),('100004','Shohrawardee','Shuvo','shohrawardee','2017-06-25','shohrawardee','01916887744',1,'shohrawardee@shohrawardee','shohrawardee shohrawardee shohrawardee','shohrawardee','shohrawardee shohrawardee','100004.jpg','2','Y','100001','2017-07-04 20:04:48',NULL,NULL,NULL,NULL),('100005','gfyj','jjghjghj','jjjhhgjgh','2017-07-21','ghjghjhg','5676574354354',3,'ytuyt@fghgfh','jgjngn','yhj','gyuytut','100005.png','1','Y','100001','2017-07-04 20:17:20',NULL,NULL,NULL,NULL),('100006','gjhhg','jhgjhg','jhgjhgj','2017-07-26','yujuytuyt','01715884477',2,'hgjfjh@fgfh','ghjhgjh','fhfghg','gjgjhg','100006.png','3','Y','100001','2017-07-04 20:18:05',NULL,NULL,NULL,NULL),('100007','hujkh','khkh','khk','2017-07-04','hujkjhk','01715884477',3,'huyjkhk@fhgjgjg','ghjhgjhg','jhgjhgj','ghjhgj','100007.png','3','Y','100001','2017-07-04 20:18:41',NULL,NULL,NULL,NULL),('100008','hujk','jhkjhkjh','kjhkjhk','2017-07-28','hjkjh','01715884477',1,'hjkhj@hgjgh','hhh','jhgyujhgjhg','hh','100008.png','3','Y','100001','2017-07-04 20:19:24',NULL,NULL,NULL,NULL),('100009','rtytrfy','trytry','trytrytrytr','2017-07-26','rtytry','54654654656',1,'dsfdsfds@fdgfdgfd','fdgfdgfd','dfgfdg','fdgfdgfdg','100009.jpg','3','Y','100004','2017-07-08 20:46:18',NULL,NULL,NULL,NULL);

/*Table structure for table `user_level_menu` */

DROP TABLE IF EXISTS `user_level_menu`;

CREATE TABLE `user_level_menu` (
  `USER_LEVEL_MENU_ID` int(11) NOT NULL auto_increment,
  `MENU_ID` int(11) default NULL,
  `GROUP_ID` int(11) default NULL,
  `USER_LEVEL_MENU_STATUS` char(1) default 'Y',
  `INSERT_BY` varchar(30) default NULL,
  `INSERT_DATE` timestamp NULL default NULL,
  `UPDATE_BY` varchar(30) default NULL,
  `UPDATE_DATE` timestamp NULL default NULL,
  PRIMARY KEY  (`USER_LEVEL_MENU_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `user_level_menu` */

insert  into `user_level_menu`(`USER_LEVEL_MENU_ID`,`MENU_ID`,`GROUP_ID`,`USER_LEVEL_MENU_STATUS`,`INSERT_BY`,`INSERT_DATE`,`UPDATE_BY`,`UPDATE_DATE`) values (1,1,0,'Y','Admin','2017-06-22 10:26:04',NULL,NULL),(2,2,0,'Y','Admin','2017-06-22 10:26:04',NULL,NULL),(3,3,0,'Y','Admin','2017-06-22 10:26:04',NULL,NULL),(4,4,0,'Y','Admin','2017-06-22 10:26:04',NULL,NULL),(5,5,0,'Y','Admin','2017-06-22 10:26:04',NULL,NULL),(6,6,0,'Y','Admin','2017-06-22 10:26:04',NULL,NULL),(7,7,0,'Y','Admin','2017-06-22 10:26:04',NULL,NULL),(8,8,0,'Y','Admin','2017-06-22 10:26:05',NULL,NULL),(9,9,0,'Y','Admin','2017-06-22 10:26:05',NULL,NULL),(10,10,0,'Y','Admin','2017-06-22 10:26:05',NULL,NULL),(11,11,0,'Y','Admin','2017-06-22 10:26:05',NULL,NULL),(12,12,0,'Y','Admin','2017-06-22 10:26:05',NULL,NULL),(13,13,0,'Y','Admin','2017-06-22 10:26:05',NULL,NULL),(14,14,0,'Y','Admin','2017-06-22 10:26:05',NULL,NULL),(15,15,0,'Y','Admin','2017-06-22 10:26:05',NULL,NULL),(16,16,0,'Y','Admin','2017-06-22 10:26:05',NULL,NULL),(17,17,0,'Y','Admin','2017-06-22 10:26:05',NULL,NULL),(18,18,0,'Y','Admin','2017-06-22 10:26:05',NULL,NULL),(19,19,0,'Y','Admin','2017-06-22 10:26:05',NULL,NULL),(20,20,0,'Y','Admin','2017-06-22 10:26:05',NULL,NULL),(21,21,0,'Y','Admin','2017-06-22 10:26:05',NULL,NULL),(22,22,0,'Y','Admin','2017-06-22 10:26:05',NULL,NULL),(23,1,1,'Y','Admin','2017-06-22 10:26:39',NULL,NULL),(24,2,1,'N','Admin','2017-06-22 10:26:39',NULL,NULL),(25,3,1,'N','Admin','2017-06-22 10:26:39',NULL,NULL),(26,4,1,'N','Admin','2017-06-22 10:26:39',NULL,NULL),(27,5,1,'N','Admin','2017-06-22 10:26:39',NULL,NULL),(28,6,1,'N','Admin','2017-06-22 10:26:39',NULL,NULL),(29,7,1,'N','Admin','2017-06-22 10:26:39',NULL,NULL),(30,8,1,'N','Admin','2017-06-22 10:26:39',NULL,NULL),(31,9,1,'N','Admin','2017-06-22 10:26:39',NULL,NULL),(32,10,1,'Y','Admin','2017-06-22 10:26:39',NULL,NULL),(33,11,1,'Y','Admin','2017-06-22 10:26:39',NULL,NULL),(34,12,1,'Y','Admin','2017-06-22 10:26:39',NULL,NULL),(35,13,1,'Y','Admin','2017-06-22 10:26:39',NULL,NULL),(36,14,1,'Y','Admin','2017-06-22 10:26:39',NULL,NULL),(37,15,1,'Y','Admin','2017-06-22 10:26:39',NULL,NULL),(38,16,1,'Y','Admin','2017-06-22 10:26:39',NULL,NULL),(39,17,1,'Y','Admin','2017-06-22 10:26:39',NULL,NULL),(40,18,1,'Y','Admin','2017-06-22 10:26:39',NULL,NULL),(41,19,1,'Y','Admin','2017-06-22 10:26:40',NULL,NULL),(42,20,1,'Y','Admin','2017-06-22 10:26:40',NULL,NULL),(43,21,1,'Y','Admin','2017-06-22 10:26:40',NULL,NULL),(44,22,1,'Y','Admin','2017-06-22 10:26:40',NULL,NULL),(45,1,2,'Y','Admin','2017-06-22 10:28:31','Admin','2017-06-22 11:36:37'),(46,2,2,'N','Admin','2017-06-22 10:28:31','Admin','2017-06-22 11:36:37'),(47,3,2,'N','Admin','2017-06-22 10:28:31','Admin','2017-06-22 11:36:37'),(48,4,2,'N','Admin','2017-06-22 10:28:32','Admin','2017-06-22 11:36:37'),(49,5,2,'N','Admin','2017-06-22 10:28:32','Admin','2017-06-22 11:36:37'),(50,6,2,'N','Admin','2017-06-22 10:28:32','Admin','2017-06-22 11:36:37'),(51,7,2,'N','Admin','2017-06-22 10:28:32','Admin','2017-06-22 11:36:37'),(52,8,2,'N','Admin','2017-06-22 10:28:32','Admin','2017-06-22 11:36:37'),(53,9,2,'N','Admin','2017-06-22 10:28:32','Admin','2017-06-22 11:36:37'),(54,10,2,'Y','Admin','2017-06-22 10:28:32','Admin','2017-06-22 11:36:37'),(55,11,2,'Y','Admin','2017-06-22 10:28:32','Admin','2017-06-22 11:36:37'),(56,12,2,'Y','Admin','2017-06-22 10:28:32','Admin','2017-06-22 11:36:37'),(57,13,2,'Y','Admin','2017-06-22 10:28:32','Admin','2017-06-22 11:36:37'),(58,14,2,'Y','Admin','2017-06-22 10:28:32','Admin','2017-06-22 11:36:37'),(59,15,2,'N','Admin','2017-06-22 10:28:32','Admin','2017-06-22 11:36:37'),(60,16,2,'N','Admin','2017-06-22 10:28:32','Admin','2017-06-22 11:36:37'),(61,17,2,'N','Admin','2017-06-22 10:28:32','Admin','2017-06-22 11:36:37'),(62,18,2,'Y','Admin','2017-06-22 10:28:32','Admin','2017-06-22 11:36:37'),(63,19,2,'Y','Admin','2017-06-22 10:28:32','Admin','2017-06-22 11:36:37'),(64,20,2,'Y','Admin','2017-06-22 10:28:32','Admin','2017-06-22 11:36:37'),(65,21,2,'Y','Admin','2017-06-22 10:28:32','Admin','2017-06-22 11:36:37'),(66,22,2,'Y','Admin','2017-06-22 10:28:32','Admin','2017-06-22 11:36:37'),(67,1,3,'N','Admin','2017-06-22 10:29:04',NULL,NULL),(68,2,3,'N','Admin','2017-06-22 10:29:04',NULL,NULL),(69,3,3,'N','Admin','2017-06-22 10:29:04',NULL,NULL),(70,4,3,'N','Admin','2017-06-22 10:29:04',NULL,NULL),(71,5,3,'N','Admin','2017-06-22 10:29:04',NULL,NULL),(72,6,3,'N','Admin','2017-06-22 10:29:04',NULL,NULL),(73,7,3,'N','Admin','2017-06-22 10:29:04',NULL,NULL),(74,8,3,'N','Admin','2017-06-22 10:29:04',NULL,NULL),(75,9,3,'N','Admin','2017-06-22 10:29:04',NULL,NULL),(76,10,3,'Y','Admin','2017-06-22 10:29:04',NULL,NULL),(77,11,3,'N','Admin','2017-06-22 10:29:04',NULL,NULL),(78,12,3,'N','Admin','2017-06-22 10:29:04',NULL,NULL),(79,13,3,'N','Admin','2017-06-22 10:29:04',NULL,NULL),(80,14,3,'Y','Admin','2017-06-22 10:29:04',NULL,NULL),(81,15,3,'N','Admin','2017-06-22 10:29:04',NULL,NULL),(82,16,3,'N','Admin','2017-06-22 10:29:04',NULL,NULL),(83,17,3,'N','Admin','2017-06-22 10:29:04',NULL,NULL),(84,18,3,'N','Admin','2017-06-22 10:29:04',NULL,NULL),(85,19,3,'N','Admin','2017-06-22 10:29:04',NULL,NULL),(86,20,3,'N','Admin','2017-06-22 10:29:04',NULL,NULL),(87,21,3,'Y','Admin','2017-06-22 10:29:04',NULL,NULL),(88,22,3,'Y','Admin','2017-06-22 10:29:04',NULL,NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
